<?php
include 'header.php';
include 'sidebar.php';
    $user_id = $_SESSION['user_id'];
    $query = "SELECT * FROM  `qwiklee_users` WHERE `user_id` = '$user_id' LIMIT 1";
    $result = mysqli_query($connection_obj,$query);
    $UserRow = mysqli_fetch_object($result);

    $qb_token = isset($_POST['qb_token'])?$_POST['qb_token']:$UserRow->qb_token;
    $qb_consumer_key = isset($_POST['qb_consumer_key'])?$_POST['qb_consumer_key']:$UserRow->qb_consumer_key;
    $qb_consumer_secret = isset($_POST['qb_consumer_secret'])?$_POST['qb_consumer_secret']:$UserRow->qb_consumer_secret;
    $google_sheet_id = isset($_POST['google_sheet_id'])?$_POST['google_sheet_id']:$UserRow->google_sheet_id;
    $customers_gid = isset($_POST['customers_gid'])?$_POST['customers_gid']:$UserRow->customers_gid;
    $vendors_gid = isset($_POST['vendors_gid'])?$_POST['vendors_gid']:$UserRow->vendors_gid;
    $invoices_gid = isset($_POST['invoices_gid'])?$_POST['invoices_gid']:$UserRow->invoices_gid;
    $sales_receipts_gid = isset($_POST['sales_receipts_gid'])?$_POST['sales_receipts_gid']:$UserRow->sales_receipts_gid;
    $update_query = "UPDATE `qwiklee_users` SET
                        `qb_token`='$qb_token',
                        `qb_consumer_key`='$qb_consumer_key',
                        `qb_consumer_secret`='$qb_consumer_secret',
                        `google_sheet_id`='$google_sheet_id',
                        `customers_gid`='$customers_gid',
                        `vendors_gid`='$vendors_gid',
                        `invoices_gid`='$invoices_gid',
                        `sales_receipts_gid`='$sales_receipts_gid'
                        WHERE `user_id` = '$user_id'";
        mysqli_query($connection_obj,$update_query);
    if(isset($_POST['qb_token']) && $_POST['qb_token'] != ''){
        $_SESSION['qb_token'] = $_POST['qb_token'];
        $_SESSION['qb_consumer_key'] = $_POST['qb_consumer_key'];
        $_SESSION['qb_consumer_secret'] = $_POST['qb_consumer_secret'];
        $_SESSION['google_sheet_id'] = $_POST['google_sheet_id'];
        $_SESSION['customers_gid'] = $_POST['customers_gid'];
        $_SESSION['vendors_gid'] = $_POST['vendors_gid'];
        $_SESSION['invoices_gid'] = $_POST['invoices_gid'];
        $_SESSION['sales_receipts_gid'] = $_POST['sales_receipts_gid'];
        $message = 'Configuration Successfully Saved!';
    }else{
        $message = '';
    }
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php
                if($message != ''){
                    ?>
                    <div class="callout callout-success">
                        <h4><?=$message;?></h4>
                    </div>
                    <?php
                    $message = '';
                }
                ?>
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Set Configuration</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="" method="post">
                        <input type="hidden" name="user_id" value="<?=$user_id;?>">
                        <div class="box-body">
                            <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Sample Google Sheet</label>
                                    <div class="col-sm-9">
                                        <a href="https://docs.google.com/spreadsheets/d/1Ensmtzv0dqIMg6VwSrtLlF0qk8gX0E1FPY7fRj2uoFQ/edit?usp=sharing" target="_blank">https://docs.google.com/spreadsheets/d/1Ensmtzv0dqIMg6VwSrtLlF0qk8gX0E1FPY7fRj2uoFQ/edit?usp=sharing</a>
                                    </div>
                            </div>
                            <div class="clearfix"></div>
                            <br/>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Google Sheet Id</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="google_sheet_id" name="google_sheet_id" value="<?=$google_sheet_id;?>" placeholder="Google Sheet Id" required="required">
                                    <br/>
                                    Like, From : https://docs.google.com/spreadsheets/d/1mWLJ8Mq7KEWG8PoB59W5oZCjOslzBIN0aWSmsayYih0/edit#gid=0
                                    <br/>
                                    => 1mWLJ8Mq7KEWG8PoB59W5oZCjOslzBIN0aWSmsayYih0
                                    <br/>
                                    <br/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Customers gid</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="customers_gid" name="customers_gid" value="<?=$customers_gid;?>" placeholder="Customers gid" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Vendors gid</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="vendors_gid" name="vendors_gid" value="<?=$vendors_gid;?>" placeholder="Vendors gid" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Invoices gid</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="invoices_gid" name="invoices_gid" value="<?=$invoices_gid;?>" placeholder="Invoices gid" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Sales Receipts gid</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="sales_receipts_gid" name="sales_receipts_gid" value="<?=$sales_receipts_gid;?>" placeholder="Sales Receipts gid" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">QB Token</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="qb_token" name="qb_token" value="<?=$qb_token;?>" placeholder="Token" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">QB Oauth Consumer Key</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="qb_consumer_key" name="qb_consumer_key" value="<?=$qb_consumer_key;?>" placeholder="Oauth Consumer Key"  required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">QB Oauth Consumer Secret</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="qb_consumer_secret" name="qb_consumer_secret" value="<?=$qb_consumer_secret;?>" placeholder="Oauth Consumer Secret" required="required">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                            <div>

                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">QB Developer Url : <a href="https://developer.intuit.com/v2/ui#/app/dashboard" target="_blank">https://developer.intuit.com/v2/ui#/app/dashboard</a></h3>
                    </div>
                    <div class="box-body with-border" style="border-top:1px solid;">
                        <img src="<?=BASEURL?>dist/img/qbDeveloper.png" width="1000">
                    </div>
                </div
                <!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include 'footer.php';
?>
