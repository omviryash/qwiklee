<?php
include 'qwiklee_config.php';
require_once __DIR__ . '/vendor/autoload.php';
define('SCOPES',implode(' ',array(
        Google_Service_Drive::DRIVE,
        Google_Service_Drive::DRIVE_FILE,
        Google_Service_Drive::DRIVE_APPDATA,
        Google_Service_Drive::DRIVE_PHOTOS_READONLY,
        )
));
$client = new Google_Client();

$client->setAuthConfigFile('client_secret.json');
$client->setAccessType("offline");
if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
    $client->setAccessToken($_SESSION['access_token']);
    $client->setScopes(SCOPES);
    $service = new Google_Service_Drive($client);

    $DriveFile = new Google_Service_Drive_DriveFile();
    $DriveFile->setName('GoogleSheet Qwicklee1');
    echo "<pre>";
    $response = array();
    try {
        $response = $service->files->copy('1lQJpRjEyl3oHh6HsCPgU5C2yGoLqKZe1CUOS1xpC8uo',$DriveFile);
        print_r($response);
    } catch (Exception $e) {
        print "An error occurred: " . $e->getMessage();
    }
    /*----------- Create Google SpreadSheet ---------------*/
    /*$CreateSprSheetRequest = new Google_Service_Sheets_Spreadsheet();
    $GSProperty = new Google_Service_Sheets_SpreadsheetProperties();
    $GSProperty->setTitle('GoogleSheet Qwicklee');
    $CreateSprSheetRequest->setProperties($GSProperty);
    $response = $service->spreadsheets->create($CreateSprSheetRequest);
    $spreadsheetId = $response->getSpreadsheetId();*/
    /*----------- Create Google SpreadSheet ---------------*/

    /*$CopySheetRequest = new Google_Service_Sheets_CopySheetToAnotherSpreadsheetRequest();
    $CopySheetRequest->setDestinationSpreadsheetId($spreadsheetId);
    $response = $service->spreadsheets_sheets->copyTo('1Ensmtzv0dqIMg6VwSrtLlF0qk8gX0E1FPY7fRj2uoFQ','0',$CopySheetRequest);*/
}else {
    $redirect_uri = BASEURL . 'logout.php';
    echo '<script>window.location.replace("' . $redirect_uri . '");</script>';
}