-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 27, 2016 at 07:15 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quickbooks_server`
--

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_customer`
--

CREATE TABLE IF NOT EXISTS `qb_example_customer` (
  `ListID` varchar(40) NOT NULL,
  `TimeCreated` datetime NOT NULL,
  `TimeModified` datetime NOT NULL,
  `Name` varchar(50) NOT NULL,
  `FullName` varchar(255) NOT NULL,
  `FirstName` varchar(40) NOT NULL,
  `MiddleName` varchar(10) NOT NULL,
  `LastName` varchar(40) NOT NULL,
  `Contact` varchar(50) NOT NULL,
  `ShipAddress_Addr1` varchar(50) NOT NULL,
  `ShipAddress_Addr2` varchar(50) NOT NULL,
  `ShipAddress_City` varchar(50) NOT NULL,
  `ShipAddress_State` varchar(25) NOT NULL,
  `ShipAddress_Province` varchar(25) NOT NULL,
  `ShipAddress_PostalCode` varchar(16) NOT NULL,
  PRIMARY KEY (`ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_estimate`
--

CREATE TABLE IF NOT EXISTS `qb_example_estimate` (
  `TxnID` varchar(40) NOT NULL,
  `TimeCreated` datetime NOT NULL,
  `TimeModified` datetime NOT NULL,
  `RefNumber` varchar(16) NOT NULL,
  `Customer_ListID` varchar(40) NOT NULL,
  `Customer_FullName` varchar(255) NOT NULL,
  `ShipAddress_Addr1` varchar(50) NOT NULL,
  `ShipAddress_Addr2` varchar(50) NOT NULL,
  `ShipAddress_City` varchar(50) NOT NULL,
  `ShipAddress_State` varchar(25) NOT NULL,
  `ShipAddress_Province` varchar(25) NOT NULL,
  `ShipAddress_PostalCode` varchar(16) NOT NULL,
  `BalanceRemaining` float NOT NULL,
  PRIMARY KEY (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_estimate_lineitem`
--

CREATE TABLE IF NOT EXISTS `qb_example_estimate_lineitem` (
  `TxnID` varchar(40) NOT NULL,
  `TxnLineID` varchar(40) NOT NULL,
  `Item_ListID` varchar(40) NOT NULL,
  `Item_FullName` varchar(255) NOT NULL,
  `Descrip` text NOT NULL,
  `Quantity` int(10) unsigned NOT NULL,
  `Rate` float NOT NULL,
  PRIMARY KEY (`TxnID`,`TxnLineID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_invoice`
--

CREATE TABLE IF NOT EXISTS `qb_example_invoice` (
  `TxnID` varchar(40) NOT NULL,
  `TimeCreated` datetime NOT NULL,
  `TimeModified` datetime NOT NULL,
  `RefNumber` varchar(16) NOT NULL,
  `Customer_ListID` varchar(40) NOT NULL,
  `Customer_FullName` varchar(255) NOT NULL,
  `ShipAddress_Addr1` varchar(50) NOT NULL,
  `ShipAddress_Addr2` varchar(50) NOT NULL,
  `ShipAddress_City` varchar(50) NOT NULL,
  `ShipAddress_State` varchar(25) NOT NULL,
  `ShipAddress_Province` varchar(25) NOT NULL,
  `ShipAddress_PostalCode` varchar(16) NOT NULL,
  `BalanceRemaining` float NOT NULL,
  PRIMARY KEY (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_invoice_lineitem`
--

CREATE TABLE IF NOT EXISTS `qb_example_invoice_lineitem` (
  `TxnID` varchar(40) NOT NULL,
  `TxnLineID` varchar(40) NOT NULL,
  `Item_ListID` varchar(40) NOT NULL,
  `Item_FullName` varchar(255) NOT NULL,
  `Descrip` text NOT NULL,
  `Quantity` int(10) unsigned NOT NULL,
  `Rate` float NOT NULL,
  PRIMARY KEY (`TxnID`,`TxnLineID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_item`
--

CREATE TABLE IF NOT EXISTS `qb_example_item` (
  `ListID` varchar(40) NOT NULL,
  `TimeCreated` datetime NOT NULL,
  `TimeModified` datetime NOT NULL,
  `Name` varchar(50) NOT NULL,
  `FullName` varchar(255) NOT NULL,
  `Type` varchar(40) NOT NULL,
  `Parent_ListID` varchar(40) NOT NULL,
  `Parent_FullName` varchar(255) NOT NULL,
  `ManufacturerPartNumber` varchar(40) NOT NULL,
  `SalesTaxCode_ListID` varchar(40) NOT NULL,
  `SalesTaxCode_FullName` varchar(255) NOT NULL,
  `BuildPoint` varchar(40) NOT NULL,
  `ReorderPoint` varchar(40) NOT NULL,
  `QuantityOnHand` int(10) unsigned NOT NULL,
  `AverageCost` float NOT NULL,
  `QuantityOnOrder` int(10) unsigned NOT NULL,
  `QuantityOnSalesOrder` int(10) unsigned NOT NULL,
  `TaxRate` varchar(40) NOT NULL,
  `SalesPrice` float NOT NULL,
  `SalesDesc` text NOT NULL,
  `PurchaseCost` float NOT NULL,
  `PurchaseDesc` text NOT NULL,
  `PrefVendor_ListID` varchar(40) NOT NULL,
  `PrefVendor_FullName` varchar(255) NOT NULL,
  PRIMARY KEY (`ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_salesorder`
--

CREATE TABLE IF NOT EXISTS `qb_example_salesorder` (
  `TxnID` varchar(40) NOT NULL,
  `TimeCreated` datetime NOT NULL,
  `TimeModified` datetime NOT NULL,
  `RefNumber` varchar(16) NOT NULL,
  `Customer_ListID` varchar(40) NOT NULL,
  `Customer_FullName` varchar(255) NOT NULL,
  `ShipAddress_Addr1` varchar(50) NOT NULL,
  `ShipAddress_Addr2` varchar(50) NOT NULL,
  `ShipAddress_City` varchar(50) NOT NULL,
  `ShipAddress_State` varchar(25) NOT NULL,
  `ShipAddress_Province` varchar(25) NOT NULL,
  `ShipAddress_PostalCode` varchar(16) NOT NULL,
  `BalanceRemaining` float NOT NULL,
  PRIMARY KEY (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_salesorder_lineitem`
--

CREATE TABLE IF NOT EXISTS `qb_example_salesorder_lineitem` (
  `TxnID` varchar(40) NOT NULL,
  `TxnLineID` varchar(40) NOT NULL,
  `Item_ListID` varchar(40) NOT NULL,
  `Item_FullName` varchar(255) NOT NULL,
  `Descrip` text NOT NULL,
  `Quantity` int(10) unsigned NOT NULL,
  `Rate` float NOT NULL,
  PRIMARY KEY (`TxnID`,`TxnLineID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_config`
--

CREATE TABLE IF NOT EXISTS `quickbooks_config` (
  `quickbooks_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) NOT NULL,
  `module` varchar(40) NOT NULL,
  `cfgkey` varchar(40) NOT NULL,
  `cfgval` varchar(40) NOT NULL,
  `cfgtype` varchar(40) NOT NULL,
  `cfgopts` text NOT NULL,
  `write_datetime` datetime NOT NULL,
  `mod_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_log`
--

CREATE TABLE IF NOT EXISTS `quickbooks_log` (
  `quickbooks_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quickbooks_ticket_id` int(10) unsigned DEFAULT NULL,
  `batch` int(10) unsigned NOT NULL,
  `msg` text NOT NULL,
  `log_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_log_id`),
  KEY `quickbooks_ticket_id` (`quickbooks_ticket_id`),
  KEY `batch` (`batch`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `quickbooks_log`
--

INSERT INTO `quickbooks_log` (`quickbooks_log_id`, `quickbooks_ticket_id`, `batch`, `msg`, `log_datetime`) VALUES
(1, NULL, 0, '{"post_data":[],"0":"get_data","1":[]}', '2016-07-29 12:23:00'),
(2, NULL, 0, '{"post_data":[],"0":"get_data","1":[]}', '2016-07-29 12:27:52'),
(3, NULL, 0, '{"post_data":[],"0":"get_data","1":[]}', '2016-07-29 12:28:02'),
(4, NULL, 0, '{"post_data":[],"get_data":[]}', '2016-09-09 07:14:30'),
(5, NULL, 0, '{"post_data":[],"get_data":[]}', '2016-09-09 07:14:34');

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_oauth`
--

CREATE TABLE IF NOT EXISTS `quickbooks_oauth` (
  `quickbooks_oauth_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app_username` varchar(255) NOT NULL,
  `app_tenant` varchar(255) NOT NULL,
  `oauth_request_token` varchar(255) DEFAULT NULL,
  `oauth_request_token_secret` varchar(255) DEFAULT NULL,
  `oauth_access_token` varchar(255) DEFAULT NULL,
  `oauth_access_token_secret` varchar(255) DEFAULT NULL,
  `qb_realm` varchar(32) DEFAULT NULL,
  `qb_flavor` varchar(12) DEFAULT NULL,
  `qb_user` varchar(64) DEFAULT NULL,
  `request_datetime` datetime NOT NULL,
  `access_datetime` datetime DEFAULT NULL,
  `touch_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`quickbooks_oauth_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `quickbooks_oauth`
--

INSERT INTO `quickbooks_oauth` (`quickbooks_oauth_id`, `app_username`, `app_tenant`, `oauth_request_token`, `oauth_request_token_secret`, `oauth_access_token`, `oauth_access_token_secret`, `qb_realm`, `qb_flavor`, `qb_user`, `request_datetime`, `access_datetime`, `touch_datetime`) VALUES
(61, '102752553874211546835', '12345', 'qyprdjHraEB14GGMTeerl2SNMPEKSLzATtAPB6zSWPP2nOlG', 'EmW7DguehDizHsIfjoc4XfFq5f5BmSsllGWWlFjR', '9SkWIgdV5HJX2oFw/qoDf5gRvbW+Lq9Q6TQJ2Yu6bXy5W5RcHD1dK0eFfk1PC3fLkOXTbf4f+zgCBYfdFThVr9s2VCQkXNPAW2lCe34cAViNztKnnZ5DWg+Y2J7GyWwg3vjP26RBe5D0KiJmh7a0FjjtTDPR2q5ZdAmlnXsxRohrDpBa25IpftFBo+hCdQ==', 'MkG7aOIoxlJ4tDcgrJ8ih8AhB5C72Rw9XiP5Ygt7SmeVtnmBO/FzYzXCKEBlXNaLrjMOLQs/A2Et+x7PGv4OnTHcrDa4H321yLiq/1Vxuz5y2fBoSdokF0QN8mdg8/NjtEJajlcbuhHrkvop4OAUF3h97CSWM2opsw3gS4ks63UUMcPH+jY=', '123145854172377', 'QBO', NULL, '2016-09-08 13:59:13', '2016-09-08 13:59:50', '2016-09-08 13:59:54'),
(64, '112152429921954155760', '12345', 'qyprd62sLZ1s8zOcQQnyKL2bEAlGJ7cANVvw3bYU8SXiw33y', 'oNYuwvKoYaIJp10UqAcsmMz8AQT2mju9Vp6pg99x', 'JUMBd0JmZSpTSmt5hyilHwBMv4Ejnd6y9YV8KOssrb7tXjEYEqJIuon3zl2gOFJzQlNWsRFX5KlKlgPQ/pJyKv9IlHxh2my0v7v4b/PX/PGaECmBExd4lCcQXTKMVGdsIjK/asjEGsPoyMl4ixppUd3W0LmhpES4J/3hIbH5DtkJy8Fdgfz6Vz382Mgo2A==', 'zp/6hbAVc6ADSq1w6jUi57iZKKnAZjCzoB6n14TDSbfUSrnE8tdfXFyi6rMD6JbO5L9h7hL6Ne2uhjPt7nvGfRqbf48pg1GzhrnnkxaS6PLNKikvA+S6CBaBwfwPUXOsI6dpsPgvJU4vXFKQCsyBb4b9qCcRMmNYzULCFXyfs5wcE0zmlIs=', '123145854172377', 'QBO', NULL, '2016-09-08 13:59:59', '2016-09-08 14:00:30', '2016-09-08 14:01:07'),
(65, 'QB_USER123', '12345', 'qyprdCVX9DDsdDIzHykaxKrzMNnQZ2iecTuNVySpG9UmmZnr', 'L4rdfaiuTzpblCOAtUnH50gAHAGx9LynfBYLGCGS', 'HZ4oDRS8PP49m+l71zqP7QArdACQ3tMXHxzArHOeG3H61RTyZ2x4AEgWmUshDS2wpH0eDhLeJZ3xwooy0qWd/ZdzG2C845fKGostJ3hZijRmgMJy2QWY5VAfHN3F+c26Jv2GiOAWoRjFkO/8AXdLPVj7U9p+6ME6rUIEPXqCAqnvLE3ZCPUywggOdNrQfw==', 'o9X4hldyhwwbrbYKOTCbTrgi3K4aHGP6/TElikccs8K/bTlDv0XdACnYNpw5xeoFvUnH5xUY9vdwWBYesR23eKQJRRd4KUW+FVk+Asjd+opvqXm/2VQJ1dsZ5HcPH/0k/23BvnZgTTjamAew92gVXpqf5niYuk56PicfimSJjDr1eUlUP+0=', '123145828399207', 'QBO', NULL, '2016-10-21 08:38:58', '2016-10-21 08:39:53', '2016-10-21 08:59:20');

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_queue`
--

CREATE TABLE IF NOT EXISTS `quickbooks_queue` (
  `quickbooks_queue_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quickbooks_ticket_id` int(10) unsigned DEFAULT NULL,
  `qb_username` varchar(40) NOT NULL,
  `qb_action` varchar(32) NOT NULL,
  `ident` varchar(40) NOT NULL,
  `extra` text,
  `qbxml` text,
  `priority` int(10) unsigned DEFAULT '0',
  `qb_status` char(1) NOT NULL,
  `msg` text,
  `enqueue_datetime` datetime NOT NULL,
  `dequeue_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`quickbooks_queue_id`),
  KEY `quickbooks_ticket_id` (`quickbooks_ticket_id`),
  KEY `priority` (`priority`),
  KEY `qb_username` (`qb_username`,`qb_action`,`ident`,`qb_status`),
  KEY `qb_status` (`qb_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `quickbooks_queue`
--

INSERT INTO `quickbooks_queue` (`quickbooks_queue_id`, `quickbooks_ticket_id`, `qb_username`, `qb_action`, `ident`, `extra`, `qbxml`, `priority`, `qb_status`, `msg`, `enqueue_datetime`, `dequeue_datetime`) VALUES
(15, NULL, '', 'CustomerAdd', '0', '', '', 0, 'q', NULL, '2016-07-27 03:14:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_recur`
--

CREATE TABLE IF NOT EXISTS `quickbooks_recur` (
  `quickbooks_recur_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) NOT NULL,
  `qb_action` varchar(32) NOT NULL,
  `ident` varchar(40) NOT NULL,
  `extra` text,
  `qbxml` text,
  `priority` int(10) unsigned DEFAULT '0',
  `run_every` int(10) unsigned NOT NULL,
  `recur_lasttime` int(10) unsigned NOT NULL,
  `enqueue_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_recur_id`),
  KEY `qb_username` (`qb_username`,`qb_action`,`ident`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_ticket`
--

CREATE TABLE IF NOT EXISTS `quickbooks_ticket` (
  `quickbooks_ticket_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) NOT NULL,
  `ticket` char(36) NOT NULL,
  `processed` int(10) unsigned DEFAULT '0',
  `lasterror_num` varchar(32) DEFAULT NULL,
  `lasterror_msg` varchar(255) DEFAULT NULL,
  `ipaddr` char(15) NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_ticket_id`),
  KEY `ticket` (`ticket`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_user`
--

CREATE TABLE IF NOT EXISTS `quickbooks_user` (
  `qb_username` varchar(40) NOT NULL,
  `qb_password` varchar(255) NOT NULL,
  `qb_company_file` varchar(255) DEFAULT NULL,
  `qbwc_wait_before_next_update` int(10) unsigned DEFAULT '0',
  `qbwc_min_run_every_n_seconds` int(10) unsigned DEFAULT '0',
  `status` char(1) NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY (`qb_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qwiklee_users`
--

CREATE TABLE `qwiklee_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `google_sheet_id` varchar(255) NOT NULL,
  `account_gid` varchar(50) NOT NULL,
  `attachable_gid` varchar(50) NOT NULL,
  `bill_gid` varchar(50) NOT NULL,
  `billpayment_gid` varchar(50) NOT NULL,
  `class_gid` varchar(50) NOT NULL,
  `creditmemo_gid` varchar(50) NOT NULL,
  `customers_gid` varchar(50) NOT NULL,
  `departmemt_gid` varchar(50) NOT NULL,
  `deposit_gid` varchar(50) NOT NULL,
  `employee_gid` varchar(50) NOT NULL,
  `estimate_gid` varchar(50) NOT NULL,
  `invoices_gid` varchar(50) NOT NULL,
  `invoice_update_gid` varchar(50) NOT NULL,
  `item_gid` varchar(50) NOT NULL,
  `journalentry_gid` varchar(50) NOT NULL,
  `payment_gid` varchar(50) NOT NULL,
  `paymentmethods_gid` varchar(50) NOT NULL,
  `purchase_gid` varchar(50) NOT NULL,
  `purchaseorders_gid` varchar(50) NOT NULL,
  `refundreceipt_gid` varchar(50) NOT NULL,
  `sales_receipts_gid` varchar(50) NOT NULL,
  `term_gid` varchar(50) NOT NULL,
  `timeactivity_gid` varchar(50) NOT NULL,
  `transfer_gid` varchar(50) NOT NULL,
  `vendors_gid` varchar(50) NOT NULL,
  `vendorcredit_gid` varchar(50) NOT NULL,
  `qb_token` varchar(255) NOT NULL,
  `qb_consumer_key` varchar(255) NOT NULL,
  `qb_consumer_secret` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Dumping data for table `qwiklee_users`
--

INSERT INTO `qwiklee_users` (`id`, `user_id`, `username`, `email`, `google_sheet_id`, `customers_gid`, `vendors_gid`, `invoices_gid`, `invoice_update_gid`, `sales_receipts_gid`, `qb_token`, `qb_consumer_key`, `qb_consumer_secret`, `updated_at`, `created_at`) VALUES
(21, '102752553874211546835', 'iGTS Developer', '', '1v09U1aErF9efdCdLMbfKFvkahQML4Ajpcv9oaCXbxBY', '2086322999', '2059789787', '272437480', '1183292726', '15456575', '', '', '', '2016-10-21 06:36:55', '2016-10-21 08:36:37');


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
