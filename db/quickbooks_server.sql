-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 19, 2016 at 01:11 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quickbooks_server`
--

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_customer`
--

CREATE TABLE IF NOT EXISTS `qb_example_customer` (
  `ListID` varchar(40) NOT NULL,
  `TimeCreated` datetime NOT NULL,
  `TimeModified` datetime NOT NULL,
  `Name` varchar(50) NOT NULL,
  `FullName` varchar(255) NOT NULL,
  `FirstName` varchar(40) NOT NULL,
  `MiddleName` varchar(10) NOT NULL,
  `LastName` varchar(40) NOT NULL,
  `Contact` varchar(50) NOT NULL,
  `ShipAddress_Addr1` varchar(50) NOT NULL,
  `ShipAddress_Addr2` varchar(50) NOT NULL,
  `ShipAddress_City` varchar(50) NOT NULL,
  `ShipAddress_State` varchar(25) NOT NULL,
  `ShipAddress_Province` varchar(25) NOT NULL,
  `ShipAddress_PostalCode` varchar(16) NOT NULL,
  PRIMARY KEY (`ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_estimate`
--

CREATE TABLE IF NOT EXISTS `qb_example_estimate` (
  `TxnID` varchar(40) NOT NULL,
  `TimeCreated` datetime NOT NULL,
  `TimeModified` datetime NOT NULL,
  `RefNumber` varchar(16) NOT NULL,
  `Customer_ListID` varchar(40) NOT NULL,
  `Customer_FullName` varchar(255) NOT NULL,
  `ShipAddress_Addr1` varchar(50) NOT NULL,
  `ShipAddress_Addr2` varchar(50) NOT NULL,
  `ShipAddress_City` varchar(50) NOT NULL,
  `ShipAddress_State` varchar(25) NOT NULL,
  `ShipAddress_Province` varchar(25) NOT NULL,
  `ShipAddress_PostalCode` varchar(16) NOT NULL,
  `BalanceRemaining` float NOT NULL,
  PRIMARY KEY (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_estimate_lineitem`
--

CREATE TABLE IF NOT EXISTS `qb_example_estimate_lineitem` (
  `TxnID` varchar(40) NOT NULL,
  `TxnLineID` varchar(40) NOT NULL,
  `Item_ListID` varchar(40) NOT NULL,
  `Item_FullName` varchar(255) NOT NULL,
  `Descrip` text NOT NULL,
  `Quantity` int(10) unsigned NOT NULL,
  `Rate` float NOT NULL,
  PRIMARY KEY (`TxnID`,`TxnLineID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_invoice`
--

CREATE TABLE IF NOT EXISTS `qb_example_invoice` (
  `TxnID` varchar(40) NOT NULL,
  `TimeCreated` datetime NOT NULL,
  `TimeModified` datetime NOT NULL,
  `RefNumber` varchar(16) NOT NULL,
  `Customer_ListID` varchar(40) NOT NULL,
  `Customer_FullName` varchar(255) NOT NULL,
  `ShipAddress_Addr1` varchar(50) NOT NULL,
  `ShipAddress_Addr2` varchar(50) NOT NULL,
  `ShipAddress_City` varchar(50) NOT NULL,
  `ShipAddress_State` varchar(25) NOT NULL,
  `ShipAddress_Province` varchar(25) NOT NULL,
  `ShipAddress_PostalCode` varchar(16) NOT NULL,
  `BalanceRemaining` float NOT NULL,
  PRIMARY KEY (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_invoice_lineitem`
--

CREATE TABLE IF NOT EXISTS `qb_example_invoice_lineitem` (
  `TxnID` varchar(40) NOT NULL,
  `TxnLineID` varchar(40) NOT NULL,
  `Item_ListID` varchar(40) NOT NULL,
  `Item_FullName` varchar(255) NOT NULL,
  `Descrip` text NOT NULL,
  `Quantity` int(10) unsigned NOT NULL,
  `Rate` float NOT NULL,
  PRIMARY KEY (`TxnID`,`TxnLineID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_item`
--

CREATE TABLE IF NOT EXISTS `qb_example_item` (
  `ListID` varchar(40) NOT NULL,
  `TimeCreated` datetime NOT NULL,
  `TimeModified` datetime NOT NULL,
  `Name` varchar(50) NOT NULL,
  `FullName` varchar(255) NOT NULL,
  `Type` varchar(40) NOT NULL,
  `Parent_ListID` varchar(40) NOT NULL,
  `Parent_FullName` varchar(255) NOT NULL,
  `ManufacturerPartNumber` varchar(40) NOT NULL,
  `SalesTaxCode_ListID` varchar(40) NOT NULL,
  `SalesTaxCode_FullName` varchar(255) NOT NULL,
  `BuildPoint` varchar(40) NOT NULL,
  `ReorderPoint` varchar(40) NOT NULL,
  `QuantityOnHand` int(10) unsigned NOT NULL,
  `AverageCost` float NOT NULL,
  `QuantityOnOrder` int(10) unsigned NOT NULL,
  `QuantityOnSalesOrder` int(10) unsigned NOT NULL,
  `TaxRate` varchar(40) NOT NULL,
  `SalesPrice` float NOT NULL,
  `SalesDesc` text NOT NULL,
  `PurchaseCost` float NOT NULL,
  `PurchaseDesc` text NOT NULL,
  `PrefVendor_ListID` varchar(40) NOT NULL,
  `PrefVendor_FullName` varchar(255) NOT NULL,
  PRIMARY KEY (`ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_salesorder`
--

CREATE TABLE IF NOT EXISTS `qb_example_salesorder` (
  `TxnID` varchar(40) NOT NULL,
  `TimeCreated` datetime NOT NULL,
  `TimeModified` datetime NOT NULL,
  `RefNumber` varchar(16) NOT NULL,
  `Customer_ListID` varchar(40) NOT NULL,
  `Customer_FullName` varchar(255) NOT NULL,
  `ShipAddress_Addr1` varchar(50) NOT NULL,
  `ShipAddress_Addr2` varchar(50) NOT NULL,
  `ShipAddress_City` varchar(50) NOT NULL,
  `ShipAddress_State` varchar(25) NOT NULL,
  `ShipAddress_Province` varchar(25) NOT NULL,
  `ShipAddress_PostalCode` varchar(16) NOT NULL,
  `BalanceRemaining` float NOT NULL,
  PRIMARY KEY (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_example_salesorder_lineitem`
--

CREATE TABLE IF NOT EXISTS `qb_example_salesorder_lineitem` (
  `TxnID` varchar(40) NOT NULL,
  `TxnLineID` varchar(40) NOT NULL,
  `Item_ListID` varchar(40) NOT NULL,
  `Item_FullName` varchar(255) NOT NULL,
  `Descrip` text NOT NULL,
  `Quantity` int(10) unsigned NOT NULL,
  `Rate` float NOT NULL,
  PRIMARY KEY (`TxnID`,`TxnLineID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_config`
--

CREATE TABLE IF NOT EXISTS `quickbooks_config` (
  `quickbooks_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) NOT NULL,
  `module` varchar(40) NOT NULL,
  `cfgkey` varchar(40) NOT NULL,
  `cfgval` varchar(40) NOT NULL,
  `cfgtype` varchar(40) NOT NULL,
  `cfgopts` text NOT NULL,
  `write_datetime` datetime NOT NULL,
  `mod_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_log`
--

CREATE TABLE IF NOT EXISTS `quickbooks_log` (
  `quickbooks_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quickbooks_ticket_id` int(10) unsigned DEFAULT NULL,
  `batch` int(10) unsigned NOT NULL,
  `msg` text NOT NULL,
  `log_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_log_id`),
  KEY `quickbooks_ticket_id` (`quickbooks_ticket_id`),
  KEY `batch` (`batch`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `quickbooks_log`
--

INSERT INTO `quickbooks_log` (`quickbooks_log_id`, `quickbooks_ticket_id`, `batch`, `msg`, `log_datetime`) VALUES
(1, NULL, 0, '{"post_data":[],"0":"get_data","1":[]}', '2016-07-29 12:23:00'),
(2, NULL, 0, '{"post_data":[],"0":"get_data","1":[]}', '2016-07-29 12:27:52'),
(3, NULL, 0, '{"post_data":[],"0":"get_data","1":[]}', '2016-07-29 12:28:02');

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_oauth`
--

CREATE TABLE IF NOT EXISTS `quickbooks_oauth` (
  `quickbooks_oauth_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app_username` varchar(255) NOT NULL,
  `app_tenant` varchar(255) NOT NULL,
  `oauth_request_token` varchar(255) DEFAULT NULL,
  `oauth_request_token_secret` varchar(255) DEFAULT NULL,
  `oauth_access_token` varchar(255) DEFAULT NULL,
  `oauth_access_token_secret` varchar(255) DEFAULT NULL,
  `qb_realm` varchar(32) DEFAULT NULL,
  `qb_flavor` varchar(12) DEFAULT NULL,
  `qb_user` varchar(64) DEFAULT NULL,
  `request_datetime` datetime NOT NULL,
  `access_datetime` datetime DEFAULT NULL,
  `touch_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`quickbooks_oauth_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `quickbooks_oauth`
--

INSERT INTO `quickbooks_oauth` (`quickbooks_oauth_id`, `app_username`, `app_tenant`, `oauth_request_token`, `oauth_request_token_secret`, `oauth_access_token`, `oauth_access_token_secret`, `qb_realm`, `qb_flavor`, `qb_user`, `request_datetime`, `access_datetime`, `touch_datetime`) VALUES
(1, 'DO_NOT_CHANGE_ME', '12345', 'qyprdFlp2RjJy9SFb4CkgxfKslo14SDAG1haG1465pkrrNOt', 'xgp7jGKnmRxX9sDeULDNUPvwISfBv7iybxZZ8SaP', 'Hlbp11Jt0lvX6sAO5nsoQSuBxCJhm58v5E920qcjSMwBhF294FuKDt+T+6BdNkpva1Fr2gbiDFFtgjZ1PwZDXJuHElkNYGfC3UHoczGhBxNh97oH+vlAxsbLPtqmYO73RfpuIApvPoh8CPVK7K0gZt28lNktF0HF8hmqRZ/J9WetEuPJd887+P6e1GCIrA==', 'H+iPVO4cSsSSQzfPhJMiuvggxcJPDq8lUjQzeCt1PDJj3V/X/Ad2VTo27WDo09SZ/MmdmKFiIhkCptxz1T4HmxGpsF0vAZx+XcuzpB1TSuXeow14B2SCK89Slk9hAQ5uXfShiQm8fUT9xC8jH3wuqB57CQQNnd/jOWO6BIRrD8DwluQR47k=', '123145828399207', 'QBO', NULL, '2016-07-26 08:39:16', '2016-07-26 08:39:39', '2016-07-26 12:51:53'),
(2, 'DO_NOT_CHANGE_ME1', '12345', 'qyprdfXSXAKiRNi0NFZAllKsg4XwQKIHJulVOtaWiKhC3ybr', 'TWI9vtCuQpsI0aeAhVEj0oYRN3YlYCGMrj5UJ1a3', NULL, NULL, NULL, NULL, NULL, '2016-07-26 12:57:38', NULL, NULL),
(3, 'QBUSER', '12345', 'qyprdmIMr6L9WmIcVHp0etjNCds85kjFCZMGRNHDua0IWmEU', '7YlzzlzkbmoekTAxVabJ9xaTtkYIfpw6oW4jE7Cw', '73Vsj3QxQcyylmQwanLn2bnP7IS7LmiY6UkQfW/f+GJ5vjjrtgpsfHt4Zt5FbynerCcvdBIhvZ+zkZPu9LRgRqJSxrU0xdQxFyVrxsg1Nr9o+b7IHzF0+f9NDPMmIRnamNjjqiiDdPzqsZh1sMmA/QDQJ6Z953wX5tqpamzU+n4mEllfT/CM40qve/njcQ==', 'WItHhSHep6vyrzfbmCX5krYlWJQQ92jxMYsyUG1yOxRycjN2Np2DlZxPSLvKU7cCcLRNTLC+tgQGPHNfehzNrG1PBrEslakT4gtNY8tkNiZdCOjAZ4UJMgxTK7ITiEjPwlVcw1/dalK2o98hydNEc4dQDoy+y+/izSQjX4MN+VJx8nnbfuQ=', '123145828399207', 'QBO', NULL, '2016-07-26 12:58:24', '2016-07-26 12:58:53', '2016-07-26 14:36:37'),
(4, 'QB_USER', '12345', 'qyprdv82i3peJJReyL6pn4EQWSUksBKmWqVjUs149QEBGxZ4', '9U4bnVcwNIX3brcbTT2CKhCJxb79GMxkYb06js3w', 'zZk+lSxRy74PsNAoh2iyRBzgLQzIgbrpHyfnFjly3zNpgZoMAxW/mxgr4x02Xlb/ig53IvGt2fLQRtQBP0sTBajz6f5qxbayIK7X3RBkCqmk/yv/wNcvefCeWLfcI7ySn12PTu3AW09et4dmbDJgOis8COszT4F/ErWQ2OrOdtUfIV1gYPF8SSABrNfZ4w==', 'Qk38hOC2+0CXqOyYw+FWkH36rTl/6Y4XJSOFLnhh+RblxtZGEWkWXCW0gQYRpvRxBcng+l+iJL8IlYtwszVKp/b0zn2tHWXcY5Bdovo+T8di0fkPHjpvV5xHynkBLTT75hPJzTf/DP8MddlxuzksnekLhAZ/cYYjtw+i5vX7vYqPoCjqP/c=', '123145828399207', 'QBO', NULL, '2016-08-19 13:06:52', '2016-08-13 07:32:45', '2016-08-19 13:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_queue`
--

CREATE TABLE IF NOT EXISTS `quickbooks_queue` (
  `quickbooks_queue_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quickbooks_ticket_id` int(10) unsigned DEFAULT NULL,
  `qb_username` varchar(40) NOT NULL,
  `qb_action` varchar(32) NOT NULL,
  `ident` varchar(40) NOT NULL,
  `extra` text,
  `qbxml` text,
  `priority` int(10) unsigned DEFAULT '0',
  `qb_status` char(1) NOT NULL,
  `msg` text,
  `enqueue_datetime` datetime NOT NULL,
  `dequeue_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`quickbooks_queue_id`),
  KEY `quickbooks_ticket_id` (`quickbooks_ticket_id`),
  KEY `priority` (`priority`),
  KEY `qb_username` (`qb_username`,`qb_action`,`ident`,`qb_status`),
  KEY `qb_status` (`qb_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `quickbooks_queue`
--

INSERT INTO `quickbooks_queue` (`quickbooks_queue_id`, `quickbooks_ticket_id`, `qb_username`, `qb_action`, `ident`, `extra`, `qbxml`, `priority`, `qb_status`, `msg`, `enqueue_datetime`, `dequeue_datetime`) VALUES
(15, NULL, '', 'CustomerAdd', '0', '', '', 0, 'q', NULL, '2016-07-27 03:14:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_recur`
--

CREATE TABLE IF NOT EXISTS `quickbooks_recur` (
  `quickbooks_recur_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) NOT NULL,
  `qb_action` varchar(32) NOT NULL,
  `ident` varchar(40) NOT NULL,
  `extra` text,
  `qbxml` text,
  `priority` int(10) unsigned DEFAULT '0',
  `run_every` int(10) unsigned NOT NULL,
  `recur_lasttime` int(10) unsigned NOT NULL,
  `enqueue_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_recur_id`),
  KEY `qb_username` (`qb_username`,`qb_action`,`ident`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_ticket`
--

CREATE TABLE IF NOT EXISTS `quickbooks_ticket` (
  `quickbooks_ticket_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) NOT NULL,
  `ticket` char(36) NOT NULL,
  `processed` int(10) unsigned DEFAULT '0',
  `lasterror_num` varchar(32) DEFAULT NULL,
  `lasterror_msg` varchar(255) DEFAULT NULL,
  `ipaddr` char(15) NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_ticket_id`),
  KEY `ticket` (`ticket`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_user`
--

CREATE TABLE IF NOT EXISTS `quickbooks_user` (
  `qb_username` varchar(40) NOT NULL,
  `qb_password` varchar(255) NOT NULL,
  `qb_company_file` varchar(255) DEFAULT NULL,
  `qbwc_wait_before_next_update` int(10) unsigned DEFAULT '0',
  `qbwc_min_run_every_n_seconds` int(10) unsigned DEFAULT '0',
  `status` char(1) NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY (`qb_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
