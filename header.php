<?php
    include 'qwiklee_config.php';
    if(!isset($_SESSION['is_login']) || $_SESSION['is_login'] == '' || $_SESSION['is_login'] == null){
        header('location:'.BASEURL.'index.php');
    }
    if(!isset($_SESSION['google_sheet_id']) && (strpos($_SERVER['PHP_SELF'],'set_configuration.php') === false) && (strpos($_SERVER['PHP_SELF'],'create_gs.php') === false)){
    	header('location:'.BASEURL.'set_configuration.php');
	}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Qwiklee </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?=BASEURL?>bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=BASEURL?>bootstrap/css/custom.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=BASEURL?>bootstrap/font-awesome/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?=BASEURL?>bootstrap/fonts/ionicons.min.css">

    <!--Page Specific Css-->

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?=BASEURL?>plugins/iCheck/all.css">

    <!--Page Specific Css-->

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=BASEURL?>dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=BASEURL?>dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=BASEURL?>plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?=BASEURL?>plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?=BASEURL?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?=BASEURL?>plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?=BASEURL?>plugins/daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?=BASEURL?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <link rel="stylesheet" href="<?=BASEURL?>plugins/datatables/dataTables.bootstrap.css">
    <!-- jQuery 2.2.3 -->
    <script src="<?=BASEURL?>plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?=BASEURL?>bootstrap/js/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?=BASEURL?>bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <link rel="stylesheet" href="<?=BASEURL?>plugins/datatables/jquery.dataTables.min.css" type="text/css">
    <script src="<?=BASEURL?>plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=BASEURL?>plugins/datatables/dataTables.bootstrap.min.js"></script>

    <!-- QuickBooks Js-->
    <script type="text/javascript" src="https://appcenter.intuit.com/Content/IA/intuit.ipp.anywhere.js"></script>

    <script type="text/javascript">
        intuit.ipp.anywhere.setup({
            menuProxy: '<?php print($quickbooks_menu_url); ?>',
            grantUrl: '<?php print($quickbooks_oauth_url); ?>'
        });
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">
    <header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>Qwiklee</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Qwiklee</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="javascript:void(0);" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?=isset($_SESSION['profile_pic']) && $_SESSION['profile_pic'] != ''?$_SESSION['profile_pic']:BASEURL.'dist/img/user2-160x160.jpg'?>" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?=isset($_SESSION['username']) && $_SESSION['username'] != ''?$_SESSION['username']:'Qwiklee User'?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?=isset($_SESSION['profile_pic']) && $_SESSION['profile_pic'] != ''?$_SESSION['profile_pic']:BASEURL.'dist/img/user2-160x160.jpg'?>" class="img-circle" alt="User Image">
                            <p><?=isset($_SESSION['username']) && $_SESSION['username'] != ''?$_SESSION['username']:'Qwiklee User'?></p>
                        </li>


                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?=BASEURL?>set_configuration.php" class="btn btn-default btn-flat">Set Configuration</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?=BASEURL?>logout.php" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
