<?php
    include 'qwiklee_config.php';
    require_once __DIR__ . '/vendor/autoload.php';
    define('SCOPES', implode(' ', array(
            Google_Service_Sheets::SPREADSHEETS,
            Google_Service_Oauth2::PLUS_LOGIN)
    ));
    $client = new Google_Client();
    $client->setScopes(SCOPES);
    $client->setAuthConfigFile('client_secret.json');
    $client->setRedirectUri(BASEURL.'login_gplus.php');
    $client->setAccessType('offline');
    $client->setApprovalPrompt("force");

    if (!isset($_GET['code'])) {
        $auth_url = $client->createAuthUrl();
        header('Location:'.filter_var($auth_url,FILTER_SANITIZE_URL));
    } else {
        $client->authenticate($_GET['code']);
        $google_oauth =new Google_Service_Oauth2($client);
        $_SESSION['access_token'] = $client->getAccessToken();
        $_SESSION['is_login'] = $google_oauth->userinfo->get()->id;
        $_SESSION['user_id'] = $google_oauth->userinfo->get()->id;
        $_SESSION['username'] = $google_oauth->userinfo->get()->name;
        $_SESSION['profile_pic'] = $google_oauth->userinfo->get()->picture;

        /*----------- GET USER CONFIG -------------------*/
            $user_id = $google_oauth->userinfo->get()->id;
            $query = "SELECT * FROM  `qwiklee_users` WHERE `user_id` = '$user_id' LIMIT 1";
            $result = mysql_query($query);
            if(mysql_num_rows($result) == 1) {
                $UserRow = mysql_fetch_object($result);
                $_SESSION['qb_token'] = $UserRow->qb_token;
                $_SESSION['qb_consumer_key'] = $UserRow->qb_consumer_key;
                $_SESSION['qb_consumer_secret'] = $UserRow->qb_consumer_secret;
                $_SESSION['google_sheet_id'] = $UserRow->google_sheet_id;

            }else{
                $created_at = date("Y-m-d H:i:s");
                $insert_query = "INSERT INTO `qwiklee_users`(`user_id`, `qb_token`, `qb_consumer_key`, `qb_consumer_secret`, `google_sheet_id`, `created_at`) VALUES ('$user_id','','','','','$created_at')";
                mysql_query($insert_query);
            }
        /*----------- GET USER CONFIG -------------------*/

        $_SESSION['is_qb_login'] = false;
        header('Location:'.BASEURL.'dashboard.php');
    }