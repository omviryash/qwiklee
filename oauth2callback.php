<?php
include 'qwicklee_config.php';
require_once __DIR__ . '/vendor/autoload.php';
define('SCOPES', implode(' ', array(
        Google_Service_Sheets::SPREADSHEETS)
));
session_start();
$client = new Google_Client();
$client->setScopes(SCOPES);
$client->setAuthConfigFile('client_secret.json');
$client->setRedirectUri(BASEURL.'oauth2callback.php');

if (!isset($_GET['code'])) {
    $auth_url = $client->createAuthUrl();
    header('Location:'.filter_var($auth_url,FILTER_SANITIZE_URL));
} else {
    $client->authenticate($_GET['code']);
    $_SESSION['access_token'] = $client->getAccessToken();
    header('Location:'.BASEURL.'.php');
}