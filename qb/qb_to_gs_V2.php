<?php
require_once dirname(__FILE__) . '/../header.php';
require_once dirname(__FILE__) . '/../sidebar.php';
require_once __DIR__ . '/../vendor/autoload.php';
define('SCOPES', implode(' ', array(
        Google_Service_Sheets::SPREADSHEETS)
));
$client = new Google_Client();
$client->setScopes(SCOPES);
$client->setAuthConfigFile('client_secret.json');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Sheets($client);
            $spreadsheetId = $_SESSION['google_sheet_id'];
            $requests = array();
            /**
             * Fetch User id from Google Sheet
             */
            $range = "Customers!A:A";
            $response = $service->spreadsheets_values->get($spreadsheetId,$range);
            $values = $response->getValues();
            if (count($values) == 0) {
                print "No data found.\n";
            } else {
                $header = $values[0];
                unset($values[0]);
                $UserIds = '';
                $i = 0;
                foreach ($values as $ExcelRow) {
                    foreach ($ExcelRow as $ExcelCellValue) {
                        if($ExcelCellValue != '' && is_numeric($ExcelCellValue)){
                            if($i != 0){
                                $UserIds .=",";
                            }
                            $UserIds .= $ExcelCellValue;
                            $i++;
                        }
                    }
                }
            }
            $UserIdsArr = explode(',',$UserIds);
            /**
             * /Fetch User id from Google Sheet
             */

            $CustomerService = new QuickBooks_IPP_Service_Customer();
            $CustomerDataList = $CustomerService->query($Context,$realm,"SELECT * FROM Customer ORDER BY Id");
            echo "<pre>";
            $user_ids = '';
            if(is_array($CustomerDataList)) {
                $i = 1;
                foreach ($CustomerDataList as $CustomerData) {
                    $user_id = trim($CustomerData->getId(),'{-}');
                    if(in_array($user_id,$UserIdsArr)){
                        $ApplyStyle = false;
                    }else{
                        $ApplyStyle = true;
                    }
                    if($ApplyStyle){
                        $requests[] = new Google_Service_Sheets_Request(array(
                            'updateDimensionProperties' => array(
                                'range' => array(
                                    'sheetId' => 0,
                                    "dimension" => "ROWS",
                                    'startIndex' => $i,
                                    'endIndex' => $i+1,
                                ),
                                "properties" => array("pixelSize" =>60),
                                'fields' => 'pixelSize'
                            )
                        ));
                    }else{
                        $requests[] = new Google_Service_Sheets_Request(array(
                            'updateDimensionProperties' => array(
                                'range' => array(
                                    'sheetId' => 0,
                                    "dimension" => "ROWS",
                                    'startIndex' => $i,
                                    'endIndex' => $i+1,
                                ),
                                "properties" => array("pixelSize" =>20),
                                'fields' => 'pixelSize'
                            )
                        ));
                    }
                    $requests[] = new Google_Service_Sheets_Request(array(
                        'updateCells' => array(
                            'start' => array(
                                'sheetId' => 0,
                                'rowIndex' => $i,
                                'columnIndex' => 0
                            ),
                            'rows' => array(array(
                                'values' => array(
                                    getCellValues(trim($CustomerData->getId(),'{-}'),$ApplyStyle),
                                    getCellValues($CustomerData->getTitle()?$CustomerData->getTitle():' ',$ApplyStyle),
                                    getCellValues($CustomerData->getGivenName()?$CustomerData->getGivenName():' ',$ApplyStyle),
                                    getCellValues($CustomerData->getMiddleName()?$CustomerData->getMiddleName():' ',$ApplyStyle),
                                    getCellValues($CustomerData->getFamilyName()?$CustomerData->getFamilyName():' ',$ApplyStyle),
                                    getCellValues($CustomerData->getSuffix()?$CustomerData->getSuffix():' ',$ApplyStyle),
                                    getCellValues($CustomerData->getDisplayName()?$CustomerData->getDisplayName():' ',$ApplyStyle),
                                    getCellValues($CustomerData->getPrimaryEmailAddr()?$CustomerData->getPrimaryEmailAddr()->getAddress():' ',$ApplyStyle),
                                    getCellValues($CustomerData->getPrimaryPhone()?$CustomerData->getPrimaryPhone()->getFreeFormNumber():' ',$ApplyStyle),
                                    getCellValues($CustomerData->getMobile()?$CustomerData->getPrimaryPhone()->getFreeFormNumber():' ',$ApplyStyle),
                                    getCellValues($CustomerData->getBillAddr()?$CustomerData->getBillAddr()->getLine1():' ',$ApplyStyle),
                                    getCellValues($CustomerData->getBillAddr() && $CustomerData->getBillAddr()->getLine2()?$CustomerData->getBillAddr()->getLine2():' ',$ApplyStyle),
                                    getCellValues($CustomerData->getBillAddr()?$CustomerData->getBillAddr()->getCity():' ',$ApplyStyle),
                                    getCellValues($CustomerData->getBillAddr() && $CustomerData->getBillAddr()->getCountrySubDivisionCode()?$CustomerData->getBillAddr()->getCountrySubDivisionCode():' ',$ApplyStyle),
                                    getCellValues($CustomerData->getBillAddr() && $CustomerData->getBillAddr()->getPostalCode()?$CustomerData->getBillAddr()->getPostalCode():' ',$ApplyStyle),
                                )
                            )
                            ),
                            'fields' => '*'
                        )
                    ));
                    $i++;
                }
                $requests[] = new Google_Service_Sheets_Request(array(
                    "deleteDimension" => array(
                        "range" => array(
                            'sheetId' => 0,
                            'dimension' => 'ROWS',
                            'startIndex' => $i+1
                        )
                    )
                ));

                $requests[] = new Google_Service_Sheets_Request(array(
                    "insertDimension" => array(
                        "range" => array(
                            'sheetId' => 0,
                            'dimension' => 'ROWS',
                            'startIndex' => $i,
                            'endIndex' => 1000
                        )
                    )
                ));
            }
            $batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest(array(
                'requests' => $requests
            ));
            $service->spreadsheets->batchUpdate($spreadsheetId,$batchUpdateRequest);
            ?>
                <div class="callout callout-success">
                    <h4>QuickBooks To Google Sheet Data Successfully Saved!</h4>
                </div>
            <?php

        } else {
            $redirect_uri = BASEURL.'logout.php';
            echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
        }
    ?>
</div>
<!-- /.content-wrapper -->
<?php
require_once dirname(__FILE__) . '/../footer.php';
/**
 * @param $fieldValue
 * @param bool $styleApply
 * @return array
 */
function getCellValues($fieldValue,$styleApply = true){
    if($styleApply){
        return array(
            'userEnteredValue' => array('stringValue' => $fieldValue),
            'userEnteredFormat' => array(
                'backgroundColor' => array('red' => 0.3,'blue' => 0.3,'green' => 0.3),
                'horizontalAlignment' => 'LEFT',
                'textFormat' => array(
                    'foregroundColor' => array('red' => 1,'blue' => 1,'green' => 1),
                ),
                'borders' => array(
                    'top' => array('style'=>'SOLID','width'=>3,'color' => array('red' =>0.01,'blue' => 0.01,'green' => 0.01)),
                    'right' => array('style'=>'SOLID','width'=>3,'color' => array('red' =>0.01,'blue' =>0.01,'green' => 0.01)),
                    'bottom' => array('style'=>'SOLID','width'=>3,'color' => array('red' => 0.01,'blue' =>0.01,'green' => 0.01)),
                    'left' => array('style'=>'SOLID','width'=>3,'color' => array('red' => 0.01,'blue' =>0.01,'green' => 0.01))
                )
            )
        );
    }else{
        return array('userEnteredValue' => array('stringValue' => $fieldValue));
    }
}
function getRowStyle($styleApply = true){
    if($styleApply){
        return array(
            'range' => array(
                'sheetId' => 0,
                "dimension" => "ROWS",
                'startIndex' => 0,
            ),
            "properties" => array("pixelSize" =>60),
            'fields' => 'pixelSize'
        );
    }else{
        return array(
            'range' => array(
                'sheetId' => 0,
                "dimension" => "ROWS",
                'startIndex' => 0,
            ),
            "properties" => array("pixelSize" =>20),
            'fields' => 'pixelSize'
        );
    }
}