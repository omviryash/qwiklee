<?php
require_once dirname(__FILE__) . '/../header.php';
require_once dirname(__FILE__) . '/../sidebar.php';
require_once __DIR__ . '/../vendor/autoload.php';
if(!isset($_GET['invoice_id'])){
    $redirect_uri = BASEURL.'qb/invoices_view.php';
    echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
}else{
    $invoice_id = $_GET['invoice_id'];
}
define('SCOPES', implode(' ', array(
        Google_Service_Sheets::SPREADSHEETS)
));
$client = new Google_Client();
$client->setScopes(SCOPES);
$client->setAuthConfigFile('client_secret.json');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header" style="padding: 15px!important;">
        <h1 style="display:inline-block;">
            Invoice
        </h1>
        <a href="<?=BASEURL.'qb/invoice_update.php?invoice_id='.$invoice_id;?>" class="btn btn-success btn-flat pull-right">Save Invoice</a>
    </section>
    <?php
    if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {

        /*--------------- Fetch Invoice Data -----------------*/

            $InvoiceService = new QuickBooks_IPP_Service_Invoice();
            $invoices = $InvoiceService->query($Context,$realm,"SELECT * FROM Invoice WHERE Id = '$invoice_id'");
            $Invoice = $invoices[0];
            $invoice_date = $Invoice->getTxnDate();
            $DocNumber = $Invoice->getDocNumber()?$Invoice->getDocNumber():' ';
            $currency = $Invoice->getCurrencyRef()?$Invoice->getCurrencyRef():' ';
            $customer = $Invoice->getCustomerRef_name()?$Invoice->getCustomerRef_name():' ';
            $DueDate  = $Invoice->getDueDate()?$Invoice->getDueDate():' ';
            $TotalAmt = $Invoice->getTotalAmt()?$Invoice->getTotalAmt():' ';
            if($Invoice->getBillEmail()){
                $BillEmail = $Invoice->getBillEmail();
                $BillEmail = $BillEmail->getAddress()?$BillEmail->getAddress():' ';
            }else{
                $BillEmail = ' ';
            }

        /*---------- Settings Google Service Sheets ---------*/

        $client->setAccessToken($_SESSION['access_token']);
        $service = new Google_Service_Sheets($client);
        $spreadsheetId = $_SESSION['google_sheet_id'];
        $SheetId = $_SESSION['invoice_update_gid'];
        $requests = array();
        $RowIndex = 9;
        $columnIndex = 2;

/*------------------------------------ Set Company Info -----------------------------------*/
        $CompanyInfoService = new QuickBooks_IPP_Service_CompanyInfo();
        $Info = $CompanyInfoService->get($Context, $realm);
        $CompanyName = $Info->getCompanyName();
        $CompanyAddress = $Info->getCompanyAddr()->getLine1();
        $CompanyCity = $Info->getCompanyAddr()->getCity();
        $CompanyPhone = $Info->getPrimaryPhone();

        $range = "InvoiceUpdate!B3:B6";
        $valueRange= new Google_Service_Sheets_ValueRange();
        $valueRange->setValues(["values"=>["$CompanyName","$CompanyAddress","$CompanyCity","$CompanyPhone"]]);
        $valueRange->setMajorDimension('COLUMNS');
        $conf = ["valueInputOption" => "RAW"];
        $service->spreadsheets_values->update($spreadsheetId,$range,$valueRange,$conf);

/*------------------------------------ First ROW ------------------------------------------*/
        /*----------- Set Invoice Date -------------*/
        $range = "InvoiceUpdate!B$RowIndex:S";
        $valueRange= new Google_Service_Sheets_ValueRange();
        $valueRange->setValues(["values"=>[$invoice_date]]);
        $conf = ["valueInputOption" => "RAW"];
        $service->spreadsheets_values->update($spreadsheetId,$range,$valueRange,$conf);
        /*----------- Set Invoice Date -------------*/

        $RowIndex=$RowIndex+3;

/*------------------------------------ Second ROW ------------------------------------------*/
        /*----------- Set Customer Name -------------*/
        $range = "InvoiceUpdate!B$RowIndex:G$RowIndex";
        $valueRange= new Google_Service_Sheets_ValueRange();
        $valueRange->setValues(["values"=>[$customer]]);
        $conf = ["valueInputOption" => "RAW"];
        $service->spreadsheets_values->update($spreadsheetId,$range,$valueRange,$conf);


        /*----------- Set Bill Email -------------*/
        $range = "InvoiceUpdate!D$RowIndex:D";
        $valueRange= new Google_Service_Sheets_ValueRange();
        $valueRange->setValues(["values"=>[$BillEmail]]);
        $conf = ["valueInputOption" => "RAW"];
        $service->spreadsheets_values->update($spreadsheetId,$range,$valueRange,$conf);


        /*----------- Set Doc Number -------------*/
        $range = "InvoiceUpdate!F$RowIndex:F";
        $valueRange= new Google_Service_Sheets_ValueRange();
        $valueRange->setValues(["values"=>[$DocNumber]]);
        $conf = ["valueInputOption" => "RAW"];
        $service->spreadsheets_values->update($spreadsheetId,$range,$valueRange,$conf);


        $RowIndex=$RowIndex+3;

/*------------------------------------ Third ROW ------------------------------------------*/

        /*----------- Set Bill Address -------------*/
        if($Invoice->getBillAddr()){
            $BillAddr = $Invoice->getBillAddr();
            $BillAddrLine1 = $BillAddr->getLine1();
            $BillAddrLine2 = $BillAddr->getLine2();
            $BillAddrLine3 = $BillAddr->getLine3();
            $BillAddrLine4 = $BillAddr->getLine4();
        }else{
            $BillAddrLine1 = $BillAddrLine2 = $BillAddrLine3 = $BillAddrLine4 = ' ';
        }
        $BillAddrRowStartIndex = $RowIndex;
        $BillAddrRowEndIndex = $BillAddrRowStartIndex+4;
        $range = "InvoiceUpdate!B$BillAddrRowStartIndex:B$BillAddrRowEndIndex";
        $valueRange= new Google_Service_Sheets_ValueRange();
        $valueRange->setValues(["values"=>["$BillAddrLine1","$BillAddrLine2","$BillAddrLine3","$BillAddrLine4"]]);
        $valueRange->setMajorDimension('COLUMNS');
        $conf = ["valueInputOption" => "RAW"];
        $service->spreadsheets_values->update($spreadsheetId,$range,$valueRange,$conf);


        /*----------- Set Ship Address -------------*/

        if($Invoice->getShipAddr()){
            $ShipAddr = $Invoice->getShipAddr();
            if(!empty($ShipAddr)){
                $ShipAddrLine1 = $ShipAddr->getLine1();
                $ShipAddrCity = $ShipAddr->getCity()?$ShipAddr->getCity():"";
                $ShipAddrPostalCode = $ShipAddr->getPostalCode()?$ShipAddr->getPostalCode():"";
            }else{
                $ShipAddrLine1 = " ";
                $ShipAddrCity = " ";
                $ShipAddrPostalCode = " ";
            }
        }else{
            $ShipAddrLine1 = " ";
            $ShipAddrCity = " ";
            $ShipAddrPostalCode = " ";
        }
        $ShipAddrRowStartIndex = $RowIndex;
        $ShipAddrRowEndIndex = $ShipAddrRowStartIndex+4;
        $range = "InvoiceUpdate!D$ShipAddrRowStartIndex:D$ShipAddrRowEndIndex";
        $valueRange= new Google_Service_Sheets_ValueRange();
        $valueRange->setValues(["values"=>["$ShipAddrLine1","$ShipAddrCity","$ShipAddrPostalCode"]]);
        $valueRange->setMajorDimension('COLUMNS');
        $conf = ["valueInputOption" => "RAW"];
        $service->spreadsheets_values->update($spreadsheetId,$range,$valueRange,$conf);

        /*----------- Set Due Date -------------*/
        $range = "InvoiceUpdate!F$RowIndex:F";
        $valueRange= new Google_Service_Sheets_ValueRange();
        $valueRange->setValues(["values"=>[$DueDate]]);
        $conf = ["valueInputOption" => "RAW"];
        $service->spreadsheets_values->update($spreadsheetId,$range,$valueRange,$conf);

        $RowIndex = $RowIndex + 7;

/*------------------------------------ Start Item Table ------------------------------------------*/
        $num_lines = $Invoice->countLine();
        for($itemcnt = 0;$itemcnt < 5;$itemcnt++){
            $Line = $Invoice->getLine($itemcnt);
            if(!empty($Line)){
                if(!empty($Line->getSalesItemLineDetail()) && $Line->getDetailType() == "SalesItemLineDetail"){
                    $ItemAmount = $Line->getAmount();
                    $ItemUnitPrice = $Line->getSalesItemLineDetail()->getUnitPrice();
                    $ItemQty = $Line->getSalesItemLineDetail()->getQty();
                    $ItemDescription = $Line->getDescription();
                }else{
                    $ItemUnitPrice = " ";
                    $ItemQty = " ";
                    $ItemDescription = " ";
                    $ItemAmount = " ";
                }
            }else{
                $ItemAmount = " ";
                $ItemUnitPrice = " ";
                $ItemQty = " ";
                $ItemDescription = " ";
            }
            $range = "InvoiceUpdate!B$RowIndex:S";
            $valueRange= new Google_Service_Sheets_ValueRange();
            $valueRange->setValues(["values"=>["$ItemDescription"," ","","$ItemQty","$ItemUnitPrice","$ItemAmount"]]);
            $conf = ["valueInputOption" => "RAW"];
            $service->spreadsheets_values->update($spreadsheetId,$range,$valueRange,$conf);
            $RowIndex++;
        }
/*------------------------------------ End Item Table ------------------------------------------*/
        $RowIndex = $RowIndex + 3;

        /*------------ Set TotalTax --------------*/
        $TotalTax = $Invoice->getTxnTaxDetail()->getTotalTax();
        $range = "InvoiceUpdate!G$RowIndex:G";
        $valueRange= new Google_Service_Sheets_ValueRange();
        $valueRange->setValues(["values"=>[$TotalTax]]);
        $conf = ["valueInputOption" => "RAW"];
        $service->spreadsheets_values->update($spreadsheetId,$range,$valueRange,$conf);
        $RowIndex++;


        /*------------ Set TotalAmt --------------*/
        $TotalAmt = $Invoice->getTotalAmt();
        $range = "InvoiceUpdate!F$RowIndex:F";
        $valueRange= new Google_Service_Sheets_ValueRange();
        $valueRange->setValues(["values"=>[$TotalAmt]]);
        $conf = ["valueInputOption" => "RAW"];
        $service->spreadsheets_values->update($spreadsheetId,$range,$valueRange,$conf);
        $RowIndex++;
    }
?>
    <iframe height="750" style="width:100%" src="https://docs.google.com/spreadsheets/d/<?=$spreadsheetId;?>/edit?rm=minimal#gid=<?=$_SESSION['invoice_update_gid'];?>"></iframe>
    <!-- /.content-wrapper -->
<?php
require_once dirname(__FILE__) . '/../footer.php';

