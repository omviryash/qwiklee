<?php
require_once dirname(__FILE__) . '/../header.php';
require_once dirname(__FILE__) . '/../sidebar.php';
require_once __DIR__ . '/../vendor/autoload.php';
if(!isset($_GET['invoice_id'])){
    $redirect_uri = BASEURL.'qb/invoices_view.php';
    echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
}else{
    $invoice_id = $_GET['invoice_id'];
}
define('SCOPES', implode(' ', array(
        Google_Service_Sheets::SPREADSHEETS)
));
$client = new Google_Client();
$client->setScopes(SCOPES);
$client->setAuthConfigFile('client_secret.json');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header" style="padding: 15px!important;">
        <h1 style="display:inline-block;">
            Invoice
        </h1>
        <a href="javascript:void(0);" class="btn btn-success btn-flat pull-right">Save Invoice</a>
    </section>
    <?php
    if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {

        /*--------------- Fetch Invoice Data -----------------*/

            $InvoiceService = new QuickBooks_IPP_Service_Invoice();
            $invoices = $InvoiceService->query($Context,$realm,"SELECT * FROM Invoice WHERE Id = '$invoice_id'");
            $Invoice = $invoices[0];
            $invoice_date = $Invoice->getTxnDate();
            $DocNumber = $Invoice->getDocNumber();
            $currency = $Invoice->getCurrencyRef();
            $customer = $Invoice->getCustomerRef_name();
            $BillAddr = $Invoice->getBillAddr();
            $ShipAddr = $Invoice->getShipAddr();
            $DueDate  = $Invoice->getDueDate();
            $TotalAmt = $Invoice->getTotalAmt();
            $BillEmail = $Invoice->getBillEmail();
            $Line = $Invoice->getLine();

        /*--------------- Fetch Invoice Data -----------------*/

        $client->setAccessToken($_SESSION['access_token']);
        $service = new Google_Service_Sheets($client);
        $spreadsheetId = $_SESSION['google_sheet_id'];
        $SheetId = $_SESSION['invoice_update_gid'];
        $requests = array();
        $RowIndex = 8;
        $columnIndex = 2;
        /*$requests[] = new Google_Service_Sheets_Request(
            array(
                'updateCells' => array(
                    'start' => array(
                        'sheetId' => $SheetId,
                        'rowIndex' => 0,
                        'columnIndex' => 0
                    ),
                    'rows' => array(
                        array(
                            'values' => array(
                                array('userEnteredValue' =>
                                    array('stringValue' => $fieldValue),
                                )
                            )
                        ),
                    'fields' => '*'
                )
            )
        ));*/
        $range = "InvoiceUpdate!A:S";
        $valueRange= new Google_Service_Sheets_ValueRange();
        $valueRange->setValues(["values"=>[$invoice_date]]);
        $conf = ["valueInputOption" => "RAW"];
        $service->spreadsheets_values->update($spreadsheetId,$range,$valueRange,$conf);

        /*$batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest(array(
            'requests' => $requests
        ));
        $response = $service->spreadsheets->batchUpdate($spreadsheetId,$batchUpdateRequest);*/
        print_r($response);
    }
?>
    <iframe height="750" style="width:100%" src="https://docs.google.com/spreadsheets/d/<?=$spreadsheetId;?>/edit?rm=minimal#gid=<?=$_SESSION['invoice_update_gid'];?>"></iframe>
    <!-- /.content-wrapper -->
<?php
require_once dirname(__FILE__) . '/../footer.php';
function setInvoiceDate($fieldValue){
    return array('userEnteredValue' => array('stringValue' => $fieldValue));
}

