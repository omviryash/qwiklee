<?php
require_once dirname(__FILE__) . '/../header.php';
require_once __DIR__ . '/../vendor/autoload.php';
if(!isset($_GET['invoice_id'])){
    $redirect_uri = BASEURL.'qb/invoices_view.php';
    echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
}else{
    $invoice_id = $_GET['invoice_id'];
}
define('SCOPES', implode(' ', array(
        Google_Service_Sheets::SPREADSHEETS)
));
$client = new Google_Client();
$client->setScopes(SCOPES);
$client->setAuthConfigFile('client_secret.json');
$client->setAccessToken($_SESSION['access_token']);
$service = new Google_Service_Sheets($client);
$spreadsheetId = $_SESSION['google_sheet_id'];
$SheetId = $_SESSION['invoice_update_gid'];
$requests = array();
$range = "InvoiceUpdate!B:G";
$response = $service->spreadsheets_values->get($spreadsheetId,$range);
$values = $response->getValues();
$RowIndex = 8;
/*---- Invoice Date -------*/
$InvoiceDate = $values[$RowIndex];

$RowIndex = $RowIndex + 3;
/*---- Second Row -------*/ //$RowIndex = 8 + 3 =  11
$CustomerName = $values[$RowIndex][0];
$BillEmail = $values[$RowIndex][2];
$DocNumber = $values[$RowIndex][4];

$RowIndex = $RowIndex + 3;
/*------------------------------------ Third ROW ------------------------------------------*/
/*---- Bill Address -------*/ //$RowIndex =11 + 3 = 14,15,16,17
$BillAddrRowIndex = $RowIndex;
$BillAddrLine1 = $values[$BillAddrRowIndex][0];
$BillAddrLine2 = $values[$BillAddrRowIndex+1][0];
$BillAddrLine3 = $values[$BillAddrRowIndex+2][0];
$BillAddrLine4 = $values[$BillAddrRowIndex+3][0];

/*---- Ship Address -------*/
$ShipAddrRowIndex = $RowIndex;
$ShipAddrLine1 = $values[$ShipAddrRowIndex][2];
$ShipAddrCity = $values[$ShipAddrRowIndex + 1][2];
$ShipAddrPostalCode = $values[$ShipAddrRowIndex + 2][2];

/*------ Due Date ----------*/
$DueDate = $values[$RowIndex][4];

$RowIndex = $RowIndex + 4 + 4;//$RowIndex = 14 + 4 + 4 = 21
/*------------------------------------ Start Item Table ------------------------------------------*/
$Line = array();
$LineIndex = 0;
while($LineIndex < 10){
    $Line[] = array(
        'desc'=>isset($values[$RowIndex][0])?$values[$RowIndex][0]:' ',
        'qty'=>isset($values[$RowIndex][3])?$values[$RowIndex][3]:' ',
        'unit_price'=>isset($values[$RowIndex][4])?$values[$RowIndex][4]:' ',
        'total_price'=>isset($values[$RowIndex][5])?$values[$RowIndex][5]:' ',
    );
    if($values[$RowIndex + 4][4] == 'TotalTax'){
        break;
    }
    $RowIndex++;
    $LineIndex++;
}
/*------------------------------------ Stop Item Table ------------------------------------------*/
$RowIndex = $RowIndex + 4;//$RowIndex = ItemsRows + 4 = 21

/*------------ Set TotalTax --------------*/
    $TotalTax = $values[$RowIndex][5];
/*------------ Set TotalAmt --------------*/
    $RowIndex++;
    $TotalAmt = $values[$RowIndex][4];

$InvoiceService = new QuickBooks_IPP_Service_Invoice();
$invoices = $InvoiceService->query($Context, $realm, "SELECT * FROM Invoice WHERE Id = '$invoice_id' ");
$Invoice = $invoices[0];
/*------- Set Invoice Date ---------*/
$Invoice->setTxnDate($InvoiceDate);

/*------- Set Bill Email ---------*/
$BillEmailObj = new QuickBooks_IPP_Object_BillEmail();
$BillEmailObj->setAddress($BillEmail);
$Invoice->setBillEmail($BillEmailObj);

/*------- Set Doc Number ---------*/
$Invoice->setDocNumber($DocNumber);

/*------- Set Bill Address ---------*/
$BillAddrObj = new QuickBooks_IPP_Object_BillAddr();
$BillAddrObj->setLine1($BillAddrLine1);
$BillAddrObj->setLine2($BillAddrLine2);
$BillAddrObj->setLine3($BillAddrLine3);
$BillAddrObj->setLine4($BillAddrLine4);
$Invoice->setBillAddr($BillAddrObj);

/*------- Set Ship Address ---------*/
$ShipAddrObj = new QuickBooks_IPP_Object_ShipAddr();
$ShipAddrObj->setLine1($ShipAddrLine1);
$ShipAddrObj->setCity($ShipAddrCity);
$ShipAddrObj->setPostalCode($ShipAddrPostalCode);
$Invoice->setShipAddr($ShipAddrObj);

/*------- Set Due Date ---------*/
$Invoice->setDueDate($DueDate);
/*------------------------------------ Start Item Table ------------------------------------------*/
/*foreach($Line as $LineRow){
    $LineObj = new QuickBooks_IPP_Object_Line();
    $LineObj->setDetailType('SalesItemLineDetail');
    $LineObj->setAmount($LineRow['unit_price'] * $LineRow['qty']);
    $LineObj->setDescription($LineRow['desc']);
    $SalesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
    $SalesItemLineDetail->setUnitPrice($LineRow['unit_price']);
    $SalesItemLineDetail->setQty($LineRow['qty']);
    $LineObj->addSalesItemLineDetail($SalesItemLineDetail);
    $Invoice->addLine($LineObj);
}*/
/*$Line = new QuickBooks_IPP_Object_Line();
$Line->setDetailType('SalesItemLineDetail');
$Line->setAmount(20.0000 * 1.0000 * 0.516129);
$Line->setDescription('Test description goes here.');

$SalesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
$SalesItemLineDetail->setItemRef('8');
$SalesItemLineDetail->setUnitPrice(20 * 0.516129);
$SalesItemLineDetail->setQty(1.00000);

$Line->addSalesItemLineDetail($SalesItemLineDetail);

$Invoice->addLine($Line);*/
/*------------------------------------ Stop Item Table ------------------------------------------*/
/*---- ---- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----*/
$resp = $InvoiceService->update($Context,$realm,$Invoice->getId(),$Invoice);
$_SESSION['message'] = 'Invoice Successfully Saved!';
$redirect_uri = BASEURL.'qb/invoice_update_view.php?invoice_id='.$invoice_id;
echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
header("location:$redirect_uri");
require_once dirname(__FILE__) . '/../footer.php';

