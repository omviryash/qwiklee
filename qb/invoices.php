<?php
ini_set('max_execution_time',300);
require_once dirname(__FILE__) . '/../qwiklee_config.php';
require_once __DIR__ . '/../vendor/autoload.php';

if(isset($_SESSION['access_token']) && $_SESSION['access_token']) {
    define('SCOPES', implode(' ', array(
            Google_Service_Sheets::SPREADSHEETS_READONLY)
    ));
    $client = new Google_Client();
    $client->setScopes(SCOPES);
    $client->setAuthConfigFile('client_secret.json');
    $client->setAccessToken($_SESSION['access_token']);
    $service = new Google_Service_Sheets($client);
    $spreadsheetId = $_SESSION['google_sheet_id'];
    $SheetId = $_SESSION['customers_gid'];
    $range = "$SheetId!A:S";
    $response = $service->spreadsheets_values->get($spreadsheetId,$range);
    $values = $response->getValues();
    if (count($values) == 0) {
        $_SESSION['message'] = 'Something wrong try again!';
        $redirect_uri = PUBLIC_URL.'qb/invoices_view.php';
        echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
    } else {
        $header = $values[0];
        unset($values[0]);
        $j = 0;
        $QbData = array();
        foreach($values as $ExcelRow){
            $i = 0;
            foreach($ExcelRow as $ExcelCellValue){
                if(isset($header[$i])){
                    $QbData[$j][$header[$i]] = $ExcelCellValue;
                }
                $i++;
            }
            $j++;
        }
        $DisplayNames = '';
        $i=0;
        foreach($QbData as $CustomerData) {
            if (isset($CustomerData['display_name_as']) && $CustomerData['display_name_as'] != '' && $CustomerData['display_name_as'] != null) {
                $CustomerService = new QuickBooks_IPP_Service_Invoice();
                $UserID = $CustomerData['user_id'];
                $DisplayName = $CustomerData['display_name_as'];
                $customers = $CustomerService->query($Context, $realm, "SELECT * FROM Customer WHERE DisplayName = '$DisplayName' ");
                if($i != 0){
                    $DisplayNames .=",";
                }
                $DisplayNames .= $DisplayName;
                $i++;
                if (isset($customers[0])) {
                    $Customer = $customers[0];
                    $response = $CustomerService->update($Context, $realm, $Customer->getId(),getUpdateCostumerObj($Customer,$CustomerData));
                } else {
                    $CustomerService->add($Context,$realm,getCostumerObj($CustomerData));
                }
            }
        }

        $DisplayNamesArray = explode(',',$DisplayNames);
        $CustomerService1 = new QuickBooks_IPP_Service_Invoice();
        $CustomerDataList = $CustomerService1->query($Context,$realm,"SELECT * FROM Customer");
        if(is_array($CustomerDataList)){
            foreach($CustomerDataList as $CustomerData)
            {
                if(!in_array($CustomerData->getDisplayName(),$DisplayNamesArray)){
                    $UserID = trim($CustomerData->getId(),'{}-');
                    $CustomerService2 = new QuickBooks_IPP_Service_Invoice();
                    $customers = $CustomerService2->query($Context, $realm, "SELECT * FROM Customer WHERE Id = '$UserID' ");
                    $Customer = $customers[0];
                    if(isset($customers[0])) {
                        $Customer = $customers[0];
                        $Customer->setActive('false');
                        $CustomerService2->update($Context, $realm, $Customer->getId(),$Customer);
                    }
                }
            }
        }
    }
    $_SESSION['message'] = 'Google Sheet To QuickBooks Data Successfully Saved!';
    $redirect_uri = BASEURL.'qb/invoices_view.php';
    echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
} else {
    $redirect_uri = BASEURL.'logout.php';
    echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
}

function getCostumerObj($CustomerData)
{
    $Customer = new QuickBooks_IPP_Object_Invoice();
    $Customer->setTitle(isset($CustomerData['title'])?$CustomerData['title']:'');
    $Customer->setGivenName(isset($CustomerData['firstname'])?$CustomerData['firstname']:'');
    $Customer->setMiddleName(isset($CustomerData['middlename'])?$CustomerData['middlename']:'');
    $Customer->setFamilyName(isset($CustomerData['lastname'])?$CustomerData['lastname']:'');
    $Customer->setSuffix(isset($CustomerData['suffix'])?$CustomerData['suffix']:'');
    $Customer->setDisplayName(isset($CustomerData['display_name_as'])?$CustomerData['display_name_as']:'');
    // Terms (e.g. Net 30, etc.)
    $Customer->setSalesTermRef(4);

    // Email
    $PrimaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
    $PrimaryEmailAddr->setAddress(isset($CustomerData['email'])?$CustomerData['email']:'');
    $Customer->setPrimaryEmailAddr($PrimaryEmailAddr);

    // Phone #
    $PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
    $PrimaryPhone->setFreeFormNumber(isset($CustomerData['phone'])?$CustomerData['phone']:'');
    $Customer->setPrimaryPhone($PrimaryPhone);
    // Mobile #
    $Mobile = new QuickBooks_IPP_Object_Mobile();
    $Mobile->setFreeFormNumber(isset($CustomerData['mobile'])?$CustomerData['mobile']:'');
    $Customer->setMobile($Mobile);

    // Bill address
    $BillAddr = new QuickBooks_IPP_Object_BillAddr();
    $BillAddr->setLine1(isset($CustomerData['address_line_1'])?$CustomerData['address_line_1']:'');
    $BillAddr->setLine2(isset($CustomerData['address_line_2'])?$CustomerData['address_line_2']:'');
    $BillAddr->setCity(isset($CustomerData['city'])?$CustomerData['city']:'');
    $BillAddr->setCountrySubDivisionCode(isset($CustomerData['country_code'])?$CustomerData['country_code']:'');
    $BillAddr->setPostalCode(isset($CustomerData['postal_code'])?$CustomerData['postal_code']:'');
    $Customer->setBillAddr($BillAddr);
    return $Customer;
}

function getUpdateCostumerObj($CustomersObj,$CustomerData)
{
    $CustomersObj->setTitle(isset($CustomerData['title'])?$CustomerData['title']:'');
    $CustomersObj->setGivenName(isset($CustomerData['firstname'])?$CustomerData['firstname']:'');
    $CustomersObj->setMiddleName(isset($CustomerData['middlename'])?$CustomerData['middlename']:'');
    $CustomersObj->setFamilyName(isset($CustomerData['lastname'])?$CustomerData['lastname']:'');
    $CustomersObj->setSuffix(isset($CustomerData['suffix'])?$CustomerData['suffix']:'');
    $CustomersObj->setDisplayName(isset($CustomerData['display_name_as'])?$CustomerData['display_name_as']:'');
    // Terms (e.g. Net 30, etc.)
    $CustomersObj->setSalesTermRef(4);
    // Email
    $PrimaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
    $PrimaryEmailAddr->setAddress(isset($CustomerData['email'])?$CustomerData['email']:'');
    $CustomersObj->setPrimaryEmailAddr($PrimaryEmailAddr);
    // Phone #
    $PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
    $PrimaryPhone->setFreeFormNumber(isset($CustomerData['phone'])?$CustomerData['phone']:'');
    $CustomersObj->setPrimaryPhone($PrimaryPhone);
    // Mobile #
    $Mobile = new QuickBooks_IPP_Object_Mobile();
    $Mobile->setFreeFormNumber(isset($CustomerData['mobile'])?$CustomerData['mobile']:'');
    $CustomersObj->setMobile($Mobile);
    // Bill address
    $BillAddr = new QuickBooks_IPP_Object_BillAddr();
    $BillAddr->setLine1(isset($CustomerData['address_line_1'])?$CustomerData['address_line_1']:'');
    $BillAddr->setLine2(isset($CustomerData['address_line_2'])?$CustomerData['address_line_2']:'');
    $BillAddr->setCity(isset($CustomerData['city'])?$CustomerData['city']:'');
    $BillAddr->setCountrySubDivisionCode(isset($CustomerData['country_code'])?$CustomerData['country_code']:'');
    $BillAddr->setPostalCode(isset($CustomerData['postal_code'])?$CustomerData['postal_code']:'');
    $CustomersObj->setBillAddr($BillAddr);
    return $CustomersObj;
}