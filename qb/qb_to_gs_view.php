<?php

require_once dirname(__FILE__) . '/../header.php';
require_once dirname(__FILE__) . '/../sidebar.php';
require_once __DIR__ . '/../vendor/autoload.php';
define('SCOPES', implode(' ', array(
        Google_Service_Sheets::SPREADSHEETS)
));
$client = new Google_Client();
$client->setScopes(SCOPES);
$client->setAuthConfigFile('client_secret.json');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="callout callout-success">
        <h4>QuickBooks To Google Sheet Data Successfully Saved!</h4>
    </div>
</div>
<!-- /.content-wrapper -->
<?php
require_once dirname(__FILE__) . '/../footer.php';