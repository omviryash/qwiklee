<?php
ini_set('max_execution_time',3000);
require_once dirname(__FILE__) . '/../qwiklee_config.php';
require_once __DIR__ . '/../vendor/autoload.php';

if(isset($_SESSION['access_token']) && $_SESSION['access_token']) {
    define('SCOPES', implode(' ', array(
            Google_Service_Sheets::SPREADSHEETS_READONLY)
    ));
    $client = new Google_Client();
    $client->setScopes(SCOPES);
    $client->setAuthConfigFile('client_secret.json');
    $client->setAccessToken($_SESSION['access_token']);
    $service = new Google_Service_Sheets($client);
    $spreadsheetId = $_SESSION['google_sheet_id'];
    $SheetId = $_SESSION['vendors_gid'];
    $range = "Vendors!A:S";
    $response = $service->spreadsheets_values->get($spreadsheetId,$range);
    $values = $response->getValues();
    echo "<pre>";
    if (count($values) == 0) {
        $_SESSION['message'] = 'Something wrong try again!';
        $redirect_uri = PUBLIC_URL.'qb/vendors_view.php';
        echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
    } else {
        $header = $values[0];
        unset($values[0]);
        $j = 0;
        $GsData = array();
        $GsUserId = array();
        $GsDispName = array();
        foreach($values as $ExcelRow){
            $i = 0;
            foreach($ExcelRow as $ExcelCellValue){
                if(isset($header[$i])){
                    $GsData[$j][$header[$i]] = $ExcelCellValue;
                }
                if($header[$i] == 'user_id'){
                    $GsUserId[] = trim($ExcelCellValue);
                }
                if($header[$i] == 'display_name_as'){
                    $GsDispName[] = trim($ExcelCellValue);
                }
                $i++;
            }
            $j++;
        }

        $VendorService = new QuickBooks_IPP_Service_Vendor();
        $QbVendorDataList = $VendorService->query($Context,$realm,"SELECT * FROM Vendor");
        $QbUserId = array();
        $QbDispName = array();
        $QBDataList = array();
        if(is_array($QbVendorDataList)){
            foreach($QbVendorDataList as $QbVenRow){
                $QbUserId[] = trim($QbVenRow->getId(),'{}-');
                $QbDispName[] = $QbVenRow->getDisplayName();
                $QBDataList[trim($QbVenRow->getId(),'{-}')] = array(
                    'user_id' => trim($QbVenRow->getId(),'{-}'),
                    'title' => $QbVenRow->getTitle()?$QbVenRow->getTitle():' ',
                    'firstname' => $QbVenRow->getGivenName()?$QbVenRow->getGivenName():' ',
                    'middlename' => $QbVenRow->getMiddleName()?$QbVenRow->getMiddleName():' ',
                    'lastname' => $QbVenRow->getFamilyName()?$QbVenRow->getFamilyName():' ',
                    'suffix' => $QbVenRow->getSuffix()?$QbVenRow->getSuffix():' ',
                    'display_name_as' => $QbVenRow->getDisplayName()?$QbVenRow->getDisplayName():' ',
                    'email' => $QbVenRow->getPrimaryEmailAddr()?$QbVenRow->getPrimaryEmailAddr()->getAddress():' ',
                    'phone' => $QbVenRow->getPrimaryPhone()?$QbVenRow->getPrimaryPhone()->getFreeFormNumber():' ',
                    'mobile' => $QbVenRow->getMobile()?$QbVenRow->getMobile()->getFreeFormNumber():' ',
                    'address_line_1' => $QbVenRow->getBillAddr()?$QbVenRow->getBillAddr()->getLine1():' ',
                    'address_line_2' => $QbVenRow->getBillAddr()?$QbVenRow->getBillAddr()->getLine2():' ',
                    'city' => $QbVenRow->getBillAddr()?$QbVenRow->getBillAddr()->getCity():' ',
                    'country_code' => $QbVenRow->getBillAddr() && $QbVenRow->getBillAddr()->getCountrySubDivisionCode()?$QbVenRow->getBillAddr()->getCountrySubDivisionCode():' ',
                    'postal_code' => $QbVenRow->getBillAddr() && $QbVenRow->getBillAddr()->getPostalCode()?$QbVenRow->getBillAddr()->getPostalCode():' ',
                );
            }
        }
        $createdUserId = array();
        $updatedUserId = array();
        $GsRowIndex = 1;
        foreach($GsData as $GsRow) {
            if(isset($GsRow['display_name_as'])){
                if(!in_array($GsRow['display_name_as'],$QbDispName)){
                    $UserId = createVendor($GsRow,$Context,$realm);
                    $GsUserId[] = $UserId;
                    $createdUserId[] = $UserId;
                }
                if(in_array($GsRow['display_name_as'],$QbDispName)){
                    //Check Record is update or not
                    if(isset($QBDataList[$GsRow['user_id']])) {
                        if (!empty(array_diff($GsRow, $QBDataList[$GsRow['user_id']]))) {
                            updateVendor($QbVendorDataList, $GsRow, $GsRow['display_name_as'], $Context, $realm);
                            $updatedUserId[] = $GsRow['user_id'];
                        }
                    }
                }
            }
            $GsRowIndex++;
        }

        $deleteUserId = array();
        foreach($QbDispName as $DispName){
            if(!in_array($DispName,$GsDispName)){
                $deleteUserId[] = $DispName;
                deleteVendor($QbVendorDataList,$DispName,$Context,$realm);
            }
        }
    }


    $VendorService = new QuickBooks_IPP_Service_Vendor();
    $QbVendorDataList = $VendorService->query($Context,$realm,"SELECT Id FROM Vendor ORDER BY Id");
    $QbUserId = array();
    if(is_array($QbVendorDataList)){
        foreach($QbVendorDataList as $QbVenRow){
            $QbUserId[] = trim($QbVenRow->getId(),'{}-');
        }
    }

    $range = "Vendors!A2:A";
    $valueRange= new Google_Service_Sheets_ValueRange();
    $valueRange->setValues(["values"=>$QbUserId]);
    $valueRange->setMajorDimension('COLUMNS');
    $conf = ["valueInputOption" => "RAW"];
    $service->spreadsheets_values->update($spreadsheetId,$range,$valueRange,$conf);

    $_SESSION['message'] = 'Google Sheet To QuickBooks Data Successfully Saved!';
    $redirect_uri = BASEURL.'qb/vendors_view.php';
    echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
} else {
    $redirect_uri = BASEURL.'logout.php';
    echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
}

/**
 * @param $VendorData
 * @param $Context
 * @param $realm
 * @return int
 */
function createVendor($VendorData,$Context,$realm){
    $Vendor = new QuickBooks_IPP_Object_Vendor();
    $Vendor->setTitle(isset($VendorData['title'])?$VendorData['title']:'');
    $Vendor->setGivenName(isset($VendorData['firstname'])?$VendorData['firstname']:'');
    $Vendor->setMiddleName(isset($VendorData['middlename'])?$VendorData['middlename']:'');
    $Vendor->setFamilyName(isset($VendorData['lastname'])?$VendorData['lastname']:'');
    $Vendor->setSuffix(isset($VendorData['suffix'])?$VendorData['suffix']:'');
    $Vendor->setDisplayName(isset($VendorData['display_name_as'])?$VendorData['display_name_as']:'');
    $Vendor->setSalesTermRef(4);

    /*------ Email ----------*/
    $PrimaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
    $PrimaryEmailAddr->setAddress(isset($VendorData['email'])?$VendorData['email']:'');
    $Vendor->setPrimaryEmailAddr($PrimaryEmailAddr);

    /*------ Phone ----------*/
    $PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
    $PrimaryPhone->setFreeFormNumber(isset($VendorData['phone'])?$VendorData['phone']:'');
    $Vendor->setPrimaryPhone($PrimaryPhone);

    // Mobile #
    $Mobile = new QuickBooks_IPP_Object_Mobile();
    $Mobile->setFreeFormNumber(isset($VendorData['mobile'])?$VendorData['mobile']:'');
    $Vendor->setMobile($Mobile);

    // Bill address
    $BillAddr = new QuickBooks_IPP_Object_BillAddr();
    $BillAddr->setLine1(isset($VendorData['address_line_1'])?$VendorData['address_line_1']:'');
    $BillAddr->setLine2(isset($VendorData['address_line_2'])?$VendorData['address_line_2']:'');
    $BillAddr->setCity(isset($VendorData['city'])?$VendorData['city']:'');
    $BillAddr->setCountrySubDivisionCode(isset($VendorData['country_code'])?$VendorData['country_code']:'');
    $BillAddr->setPostalCode(isset($VendorData['postal_code'])?$VendorData['postal_code']:'');
    $Vendor->setBillAddr($BillAddr);

    $VendorService = new QuickBooks_IPP_Service_Vendor();
    return $VendorService->add($Context,$realm,$Vendor);
}

/**
 * @param $DataList
 * @param $VendorData
 * @param $DispName
 * @param $Context
 * @param $realm
 * @return bool
 */
function updateVendor($DataList,$VendorData,$DispName,$Context,$realm){
    $Vendor = getVenObjByVenDisplayName($DataList,$DispName);
    $Vendor->setTitle(isset($VendorData['title'])?$VendorData['title']:' ');
    $Vendor->setGivenName(isset($VendorData['firstname'])?$VendorData['firstname']:' ');
    $Vendor->setMiddleName(isset($VendorData['middlename'])?$VendorData['middlename']:' ');
    $Vendor->setFamilyName(isset($VendorData['lastname'])?$VendorData['lastname']:' ');
    $Vendor->setSuffix(isset($VendorData['suffix'])?$VendorData['suffix']:' ');
    $Vendor->setDisplayName(isset($VendorData['display_name_as'])?$VendorData['display_name_as']:' ');
    $Vendor->setSalesTermRef(4);

    /*------ Email ----------*/
    $PrimaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
    $PrimaryEmailAddr->setAddress(isset($VendorData['email'])?$VendorData['email']:' ');
    $Vendor->setPrimaryEmailAddr($PrimaryEmailAddr);

    /*------ Phone ----------*/
    $PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
    $PrimaryPhone->setFreeFormNumber(isset($VendorData['phone'])?$VendorData['phone']:' ');
    $Vendor->setPrimaryPhone($PrimaryPhone);

    /*------ Mobile ----------*/
    $Mobile = new QuickBooks_IPP_Object_Mobile();
    $Mobile->setFreeFormNumber(isset($VendorData['mobile'])?$VendorData['mobile']:' ');
    $Vendor->setMobile($Mobile);

    /*------ Bill address ----------*/
    $BillAddr = new QuickBooks_IPP_Object_BillAddr();
    $BillAddr->setLine1(isset($VendorData['address_line_1'])?$VendorData['address_line_1']:' ');
    $BillAddr->setLine2(isset($VendorData['address_line_2'])?$VendorData['address_line_2']:' ');
    $BillAddr->setCity(isset($VendorData['city'])?$VendorData['city']:'');
    $BillAddr->setCountrySubDivisionCode(isset($VendorData['country_code'])?$VendorData['country_code']:' ');
    $BillAddr->setPostalCode(isset($VendorData['postal_code'])?$VendorData['postal_code']:' ');
    $Vendor->setBillAddr($BillAddr);
    $VendorService = new QuickBooks_IPP_Service_Vendor();
    return $VendorService->update($Context, $realm,$Vendor->getId(),$Vendor);
}

/**
 * @param $DataList
 * @param $DispName
 * @param $Context
 * @param $realm
 * @return bool
 */
function deleteVendor($DataList,$DispName,$Context,$realm){
    $Vendor = getVenObjByVenDisplayName($DataList,$DispName);
    $Vendor->setActive('false');
    $VendorService = new QuickBooks_IPP_Service_Vendor();
    return $VendorService->update($Context,$realm,$Vendor->getId(),$Vendor);

}

/**
 * @param $DataList
 * @param $DisplayName
 * @return mixed
 */
function getVenObjByVenDisplayName($DataList,$DisplayName){
    for($i=0;$i<count($DataList);$i++){
        if($DataList[$i]->getDisplayName() == $DisplayName){
            return $DataList[$i];
        }
    }
}