<?php
require_once dirname(__FILE__) . '/../header.php';
require_once dirname(__FILE__) . '/../sidebar.php';
require_once __DIR__ . '/../vendor/autoload.php';
define('SCOPES', implode(' ', array(
        Google_Service_Sheets::SPREADSHEETS)
));
$client = new Google_Client();
$client->setScopes(SCOPES);
$client->setAuthConfigFile('client_secret.json');
?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <?php
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Sheets($client);
            $spreadsheetId = $_SESSION['google_sheet_id'];
            $requests = array();
            /**
             * Fetch User id from Google Sheet
             */
            $SheetArray = array('Customers', 'Vendors', 'Invoices', 'SalesReceipts','Payment','Account','Attachable','Bill','BillPayment',
                                );
            for ($k = 0; $k < 9; $k++) {
                $SheetName = $SheetArray[$k];
                if ($k == 0) {
                    $SheetId = $_SESSION['customers_gid'];
                    $DataObjectList = getDataList($Context, $realm, 'Customer');
                } elseif ($k == 1) {
                    $SheetId = $_SESSION['vendors_gid'];
                    $DataObjectList = getDataList($Context, $realm, 'Vendor');
                } elseif ($k == 2) {
                    $SheetId = $_SESSION['invoices_gid'];
                    $DataObjectList = getDataList($Context, $realm, 'Invoice');
                } elseif ($k == 3) {
                    $SheetId = $_SESSION['sales_receipts_gid'];
                    $DataObjectList = getDataList($Context, $realm, 'SalesReceipt');
                } elseif ($k == 4) {
                    $SheetId = $_SESSION['payment_gid'];
                    $DataObjectList = getDataList($Context, $realm, 'Payment');
                } elseif ($k == 5) {
                    $SheetId = $_SESSION['account_gid'];
                    $DataObjectList = getDataList($Context, $realm, 'Account');
                } elseif ($k == 6){
					$SheetId = $_SESSION['attachable_gid'];
					$DataObjectList = getDataList($Context, $realm, 'Attachable'); 
                } elseif ($k == 7){
					$SheetId = $_SESSION['bill_gid'];
					$DataObjectList = getDataList($Context, $realm, 'Bill'); 
                } elseif ($k == 8){
					$SheetId = $_SESSION['billpayment_gid'];
					$DataObjectList = getDataList($Context, $realm, 'BillPayment'); 
                } else {
                    $SheetId = 0;
                    $DataObjectList = getDataList($Context, $realm, 'Customer');
                }

                /**
                 * Fetch User id from Google Sheet
                 */
                $range = "$SheetName!A:A";
                $response = $service->spreadsheets_values->get($spreadsheetId, $range);
                $values = $response->getValues();

                if (count($values) == 0) {
                } else {
                    $header = $values[0];
                    unset($values[0]);
                    $UserIds = '';
                    $i = 0;
                    foreach ($values as $ExcelRow) {
                        foreach ($ExcelRow as $ExcelCellValue) {
                            if ($ExcelCellValue != '' && is_numeric($ExcelCellValue)) {
                                if ($i != 0) {
                                    $UserIds .= ",";
                                }
                                $UserIds .= $ExcelCellValue;
                                $i++;
                            }
                        }
                    }
                }
                $UserIdsArr = explode(',', $UserIds);
                /**
                 * /Fetch User id from Google Sheet
                 */

                $user_ids = '';
                if (is_array($DataObjectList) && !empty($DataObjectList)) {
                    $i = 1;
                    $requests[] = new Google_Service_Sheets_Request(array(
                        'updateDimensionProperties' => array(
                            'range' => array(
                                'sheetId' => $SheetId,
                                "dimension" => "ROWS",
                                'startIndex' => 1,
                            ),
                            "properties" => array("pixelSize" => 20),
                            'fields' => 'pixelSize'
                        )
                    ));
                    foreach ($DataObjectList as $DataObject) {
                        $user_id = trim($DataObject->getId(), '{-}');
                        if (in_array($user_id, $UserIdsArr)) {
                            $ApplyStyle = false;
                        } else {
                            $ApplyStyle = true;
                        }
                        if ($ApplyStyle) {
                            $requests[] = new Google_Service_Sheets_Request(array(
                                'updateDimensionProperties' => array(
                                    'range' => array(
                                        'sheetId' => $SheetId,
                                        "dimension" => "ROWS",
                                        'startIndex' => $i,
                                        'endIndex' => $i + 1,
                                    ),
                                    "properties" => array("pixelSize" => 60),
                                    'fields' => 'pixelSize'
                                )
                            ));
                        }

                        if ($k == 0) {
                            $requests[] = new Google_Service_Sheets_Request(updateCustomersSheetRequest($DataObject, $i, $SheetId, $ApplyStyle));
                        } elseif ($k == 1) {
                            $requests[] = new Google_Service_Sheets_Request(updateVendorsSheetRequest($DataObject, $i, $SheetId, $ApplyStyle));
                        } elseif ($k == 2) {
                            $requests[] = new Google_Service_Sheets_Request(updateInvoicesSheetRequest($DataObject, $i, $SheetId, $ApplyStyle));
                        } elseif ($k == 3) {
                            $requests[] = new Google_Service_Sheets_Request(updateSalesReceiptsSheetRequest($DataObject, $i, $SheetId, $ApplyStyle));
                        } elseif ($k == 4) {
                            $requests[] = new Google_Service_Sheets_Request(updatePaymentSheetRequest($DataObject, $i, $SheetId, $ApplyStyle));
                        } elseif ($k == 5) {
                            $requests[] = new Google_Service_Sheets_Request(updateAccountSheetRequest($DataObject, $i, $SheetId, $ApplyStyle));
                        } elseif ($k == 6) {
                            $requests[] = new Google_Service_Sheets_Request(updateAttachableSheetRequest($DataObject, $i, $SheetId, $ApplyStyle));
                        } elseif ($k == 7) {
                            $requests[] = new Google_Service_Sheets_Request(updateBillSheetRequest($DataObject, $i, $SheetId, $ApplyStyle));
                        } elseif ($k == 8) {
                            $requests[] = new Google_Service_Sheets_Request(updateBillPaymentSheetRequest($DataObject, $i, $SheetId, $ApplyStyle));
                        } else {
                            $requests[] = new Google_Service_Sheets_Request(updateCustomersSheetRequest($DataObject, $i, $SheetId, $ApplyStyle));
                        }
                        $i++;
                    }

                    $requests[] = new Google_Service_Sheets_Request(array(
                        "deleteDimension" => array(
                            "range" => array(
                                'sheetId' => $SheetId,
                                'dimension' => 'ROWS',
                                'startIndex' => $i + 1
                            )
                        )
                    ));

                    $requests[] = new Google_Service_Sheets_Request(array(
                        "insertDimension" => array(
                            "range" => array(
                                'sheetId' => $SheetId,
                                'dimension' => 'ROWS',
                                'startIndex' => $i,
                                'endIndex' => $i + 100
                            )
                        )
                    ));
                }
                $batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest(array(
                    'requests' => $requests
                ));
                $service->spreadsheets->batchUpdate($spreadsheetId, $batchUpdateRequest);
            }
        } else {
            $redirect_uri = BASEURL . 'logout.php';
            echo '<script>window.location.replace("' . $redirect_uri . '");</script>';
        }
        ?>
        <div class="callout callout-success">
            <h4>QuickBooks To Google Sheet Data Successfully Saved!</h4>
        </div>
    </div>
    <!-- /.content-wrapper -->
<?php
require_once dirname(__FILE__) . '/../footer.php';

/**
 * @param $fieldValue
 * @param bool $styleApply
 * @return array
 */
function getCellValues($fieldValue, $styleApply = true)
{
    if ($styleApply) {
        return array(
            'userEnteredValue' => array('stringValue' => $fieldValue),
            'userEnteredFormat' => array(
                'backgroundColor' => array('red' => 0.3, 'blue' => 0.3, 'green' => 0.3),
                'horizontalAlignment' => 'LEFT',
                'textFormat' => array(
                    'foregroundColor' => array('red' => 1, 'blue' => 1, 'green' => 1),
                ),
                'borders' => array(
                    'top' => array('style' => 'SOLID', 'width' => 3, 'color' => array('red' => 0.01, 'blue' => 0.01, 'green' => 0.01)),
                    'right' => array('style' => 'SOLID', 'width' => 3, 'color' => array('red' => 0.01, 'blue' => 0.01, 'green' => 0.01)),
                    'bottom' => array('style' => 'SOLID', 'width' => 3, 'color' => array('red' => 0.01, 'blue' => 0.01, 'green' => 0.01)),
                    'left' => array('style' => 'SOLID', 'width' => 3, 'color' => array('red' => 0.01, 'blue' => 0.01, 'green' => 0.01))
                )
            )
        );
    } else {
        return array('userEnteredValue' => array('stringValue' => $fieldValue));
    }
}

function getRowStyle($styleApply = true, $SheetId = 0)
{
    if ($styleApply) {
        return array(
            'range' => array(
                'sheetId' => 0,
                "dimension" => "ROWS",
                'startIndex' => $SheetId,
            ),
            "properties" => array("pixelSize" => 60),
            'fields' => 'pixelSize'
        );
    } else {
        return array(
            'range' => array(
                'sheetId' => $SheetId,
                "dimension" => "ROWS",
                'startIndex' => 0,
            ),
            "properties" => array("pixelSize" => 20),
            'fields' => 'pixelSize'
        );
    }
}

function getDataList($Context, $realm, $table_name)
{
    $CustomerService = new QuickBooks_IPP_Service_Customer();
    $DataObjectList = $CustomerService->query($Context, $realm, "SELECT * FROM $table_name ORDER BY Id");
    return $DataObjectList;
}

function updateCustomersSheetRequest($DataObject, $i, $SheetId = 0, $ApplyStyle = false)
{
    return array(
        'updateCells' => array(
            'start' => array(
                'sheetId' => $SheetId,
                'rowIndex' => $i,
                'columnIndex' => 0
            ),
            'rows' => array(array(
                'values' => array(
                    getCellValues(trim($DataObject->getId(), '{-}'), $ApplyStyle),
                    getCellValues($DataObject->getTitle() ? $DataObject->getTitle() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getGivenName() ? $DataObject->getGivenName() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getMiddleName() ? $DataObject->getMiddleName() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getFamilyName() ? $DataObject->getFamilyName() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getSuffix() ? $DataObject->getSuffix() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getDisplayName() ? $DataObject->getDisplayName() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getPrimaryEmailAddr() ? $DataObject->getPrimaryEmailAddr()->getAddress() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getPrimaryPhone() ? $DataObject->getPrimaryPhone()->getFreeFormNumber() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getMobile() ? $DataObject->getPrimaryPhone()->getFreeFormNumber() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getBillAddr() ? $DataObject->getBillAddr()->getLine1() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getBillAddr() && $DataObject->getBillAddr()->getLine2() ? $DataObject->getBillAddr()->getLine2() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getBillAddr() ? $DataObject->getBillAddr()->getCity() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getBillAddr() && $DataObject->getBillAddr()->getCountrySubDivisionCode() ? $DataObject->getBillAddr()->getCountrySubDivisionCode() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getBillAddr() && $DataObject->getBillAddr()->getPostalCode() ? $DataObject->getBillAddr()->getPostalCode() : ' ', $ApplyStyle),
                )
            )
            ),
            'fields' => '*'
        )
    );
}

function updateVendorsSheetRequest($DataObject, $i, $SheetId = 0, $ApplyStyle = false)
{
    return array(
        'updateCells' => array(
            'start' => array(
                'sheetId' => $SheetId,
                'rowIndex' => $i,
                'columnIndex' => 0
            ),
            'rows' => array(array(
                'values' => array(
                    getCellValues(trim($DataObject->getId(), '{-}'), $ApplyStyle),
                    getCellValues($DataObject->getTitle() ? $DataObject->getTitle() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getGivenName() ? $DataObject->getGivenName() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getMiddleName() ? $DataObject->getMiddleName() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getFamilyName() ? $DataObject->getFamilyName() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getSuffix() ? $DataObject->getSuffix() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getDisplayName() ? $DataObject->getDisplayName() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getPrimaryEmailAddr() ? $DataObject->getPrimaryEmailAddr()->getAddress() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getPrimaryPhone() ? $DataObject->getPrimaryPhone()->getFreeFormNumber() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getMobile() ? $DataObject->getPrimaryPhone()->getFreeFormNumber() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getBillAddr() ? $DataObject->getBillAddr()->getLine1() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getBillAddr() && $DataObject->getBillAddr()->getLine2() ? $DataObject->getBillAddr()->getLine2() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getBillAddr() ? $DataObject->getBillAddr()->getCity() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getBillAddr() && $DataObject->getBillAddr()->getCountrySubDivisionCode() ? $DataObject->getBillAddr()->getCountrySubDivisionCode() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getBillAddr() && $DataObject->getBillAddr()->getPostalCode() ? $DataObject->getBillAddr()->getPostalCode() : ' ', $ApplyStyle),
                )
            )
            ),
            'fields' => '*'
        )
    );
}

function updateInvoicesSheetRequest($DataObject, $i, $SheetId = 0, $ApplyStyle = false)
{
    return array(
        'updateCells' => array(
            'start' => array(
                'sheetId' => $SheetId,
                'rowIndex' => $i,
                'columnIndex' => 0
            ),
            'rows' => array(array(
                'values' => array(
                    getCellValues(trim($DataObject->getId(), '{-}'), $ApplyStyle),
                    getCellValues(BASEURL . "qb/invoice_update_view.php?invoice_id=" . trim($DataObject->getId(), '{-}'), $ApplyStyle),
                    getCellValues($DataObject->getDocNumber() ? $DataObject->getDocNumber() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getCustomerRef() ? trim($DataObject->getCustomerRef(), '{-}') : ' ', $ApplyStyle),
                    getCellValues($DataObject->getCustomerRef_name() ? $DataObject->getCustomerRef_name() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getTotalAmt() ? $DataObject->getTotalAmt() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getCurrencyRef() ? trim($DataObject->getCurrencyRef(), '{-}') : ' ', $ApplyStyle),
                    getCellValues($DataObject->getDueDate() ? $DataObject->getDueDate() : ' ', $ApplyStyle),
                )
            )
            ),
            'fields' => '*'
        )
    );
}

/**
 * @param $DataObject
 * @param $i
 * @param int $SheetId
 * @param bool $ApplyStyle
 * @return array
 */
function updateSalesReceiptsSheetRequest($DataObject, $i, $SheetId = 0, $ApplyStyle = false)
{
    return array(
        'updateCells' => array(
            'start' => array(
                'sheetId' => $SheetId,
                'rowIndex' => $i,
                'columnIndex' => 0
            ),
            'rows' => array(array(
                'values' => array(
                    getCellValues(trim($DataObject->getId(), '{-}'), $ApplyStyle),
                    getCellValues($DataObject->getDocNumber() ? $DataObject->getDocNumber() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getCustomerRef() ? trim($DataObject->getCustomerRef(), '{-}') : ' ', $ApplyStyle),
                    getCellValues($DataObject->getCustomerRef_name() ? $DataObject->getCustomerRef_name() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getTotalAmt() ? $DataObject->getTotalAmt() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getCurrencyRef() ? trim($DataObject->getCurrencyRef(), '{-}') : ' ', $ApplyStyle),
                    getCellValues($DataObject->getPaymentMethodRef_name() ? $DataObject->getPaymentMethodRef_name() : ' ', $ApplyStyle),
                )
            )
            ),
            'fields' => '*'
        )
    );
}

function updatePaymentSheetRequest($DataObject, $i, $SheetId = 0, $ApplyStyle = false)
{
    return array(
        'updateCells' => array(
            'start' => array(
                'sheetId' => $SheetId,
                'rowIndex' => $i,
                'columnIndex' => 0
            ),
            'rows' => array(array(
                'values' => array(
                    getCellValues(trim($DataObject->getId(), '{-}'), $ApplyStyle),
					getCellValues($DataObject->getTxnDate() ? $DataObject->getTxnDate() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getCurrencyRef() ? trim($DataObject->getCurrencyRef(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getExchangeRate() ? $DataObject->getExchangeRate() : ' ', $ApplyStyle),
					getCellValues($DataObject->getPrivateNote() ? $DataObject->getPrivateNote() : ' ', $ApplyStyle),
					getCellValues($DataObject->getTxnStatus() ? $DataObject->getTxnStatus() : ' ', $ApplyStyle),
					getCellValues(' ', $ApplyStyle),
                    getCellValues($DataObject->getCustomerRef() ? trim($DataObject->getCustomerRef(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getCustomerRef_name() ? $DataObject->getCustomerRef_name() : ' ', $ApplyStyle),
					getCellValues($DataObject->getARAccountRef() ? $DataObject->getARAccountRef() : ' ', $ApplyStyle),
					getCellValues($DataObject->getDepositToAccountRef() ? trim($DataObject->getDepositToAccountRef(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getPaymentMethodRef() ? trim($DataObject->getPaymentMethodRef(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getPaymentRefNum() ? $DataObject->getPaymentRefNum() : ' ', $ApplyStyle),
					getCellValues($DataObject->getCreditCardPayment() ? $DataObject->getCreditCardPayment() : ' ', $ApplyStyle),
					getCellValues($DataObject->getTxnSource() ? $DataObject->getTxnSource() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getTotalAmt() ? $DataObject->getTotalAmt() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getUnappliedAmt() ? $DataObject->getUnappliedAmt() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getTransactionLocationType() ? $DataObject->getTransactionLocationType() : ' ', $ApplyStyle),
                )
            )
            ),
            'fields' => '*'
        )
    );
}
function updateAccountSheetRequest($DataObject, $i, $SheetId = 0, $ApplyStyle = false)
{
    return array(
        'updateCells' => array(
            'start' => array(
                'sheetId' => $SheetId,
                'rowIndex' => $i,
                'columnIndex' => 0
            ),
            'rows' => array(array(
                'values' => array(
                    getCellValues(trim($DataObject->getId(), '{-}'), $ApplyStyle),
					getCellValues($DataObject->getName() ? $DataObject->getName() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getSubAccount() ? trim($DataObject->getSubAccount(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getParentRef() ? $DataObject->getParentRef() : ' ', $ApplyStyle),
					getCellValues($DataObject->getDescription() ? $DataObject->getDescription() : ' ', $ApplyStyle),
					getCellValues($DataObject->getFullyQualifiedName() ? $DataObject->getFullyQualifiedName() : ' ', $ApplyStyle),
					getCellValues($DataObject->getActive() ? $DataObject->getActive() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getClassification() ? trim($DataObject->getClassification(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getAccountType() ? $DataObject->getAccountType() : ' ', $ApplyStyle),
					getCellValues($DataObject->getAccountSubType() ? $DataObject->getAccountSubType() : ' ', $ApplyStyle),
					getCellValues($DataObject->getAcctNum() ? trim($DataObject->getAcctNum(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getCurrentBalance() ? trim($DataObject->getCurrentBalance(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getCurrentBalanceWithSubAccounts() ? $DataObject->getCurrentBalanceWithSubAccounts() : ' ', $ApplyStyle),
					
                )
            )
            ),
            'fields' => '*'
        )
    );
}
function updateAttachableSheetRequest($DataObject, $i, $SheetId = 0, $ApplyStyle = false)
{
    return array(
        'updateCells' => array(
            'start' => array(
                'sheetId' => $SheetId,
                'rowIndex' => $i,
                'columnIndex' => 0
            ),
            'rows' => array(array(
                'values' => array(
                    getCellValues(trim($DataObject->getId(), '{-}'), $ApplyStyle),
					getCellValues($DataObject->getAttachableRef() ? $DataObject->getAttachableRef() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getFileName() ? trim($DataObject->getFileName(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getNote() ? $DataObject->getNotes() : ' ', $ApplyStyle),
					getCellValues($DataObject->getFileAccessUri() ? $DataObject->getFileAccessUri() : ' ', $ApplyStyle),
					getCellValues($DataObject->getTempDownloadUri() ? $DataObject->getTempDownloadUri() : ' ', $ApplyStyle),
					getCellValues($DataObject->getSize() ? $DataObject->getSize() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getContentType() ? trim($DataObject->getContentType(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getCategory() ? $DataObject->getCategory() : ' ', $ApplyStyle),
					getCellValues($DataObject->getLat() ? $DataObject->getLat() : ' ', $ApplyStyle),
					getCellValues($DataObject->getLong() ? trim($DataObject->getLong(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getPlaceName() ? trim($DataObject->getPlaceName(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getTag() ? $DataObject->getTag() : ' ', $ApplyStyle),
					getCellValues($DataObject->getThumbnailFileAccessUri() ? $DataObject->getThumbnailFileAccessUri() : ' ', $ApplyStyle),
					getCellValues($DataObject->getThumbnailTempDownloadUri() ? $DataObject->getThumbnailTempDownloadUri() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getAccountAlias() ? $DataObject->getAccountAlias() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getTxnLocationType() ? $DataObject->getTxnLocationType() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getTransactionLocationType() ? $DataObject->getTransactionLocationType() : ' ', $ApplyStyle),
                )
            )
            ),
            'fields' => '*'
        )
    );
}
function updateBillSheetRequest($DataObject, $i, $SheetId = 0, $ApplyStyle = false)
{return array(
        'updateCells' => array(
            'start' => array(
                'sheetId' => $SheetId,
                'rowIndex' => $i,
                'columnIndex' => 0
            ),
            'rows' => array(array(
                'values' => array(
                    getCellValues(trim($DataObject->getId(), '{-}'), $ApplyStyle),
					getCellValues($DataObject->getAttachableRef() ? $DataObject->getAttachableRef() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getFileName() ? trim($DataObject->getFileName(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getNote() ? $DataObject->getNotes() : ' ', $ApplyStyle),
					getCellValues($DataObject->getFileAccessUri() ? $DataObject->getFileAccessUri() : ' ', $ApplyStyle),
					getCellValues($DataObject->getTempDownloadUri() ? $DataObject->getTempDownloadUri() : ' ', $ApplyStyle),
					getCellValues($DataObject->getSize() ? $DataObject->getSize() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getContentType() ? trim($DataObject->getContentType(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getCategory() ? $DataObject->getCategory() : ' ', $ApplyStyle),
					getCellValues($DataObject->getLat() ? $DataObject->getLat() : ' ', $ApplyStyle),
					getCellValues($DataObject->getLong() ? trim($DataObject->getLong(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getPlaceName() ? trim($DataObject->getPlaceName(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getTag() ? $DataObject->getTag() : ' ', $ApplyStyle),
					getCellValues($DataObject->getThumbnailFileAccessUri() ? $DataObject->getThumbnailFileAccessUri() : ' ', $ApplyStyle),
					getCellValues($DataObject->getThumbnailTempDownloadUri() ? $DataObject->getThumbnailTempDownloadUri() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getAccountAlias() ? $DataObject->getAccountAlias() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getTxnLocationType() ? $DataObject->getTxnLocationType() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getTransactionLocationType() ? $DataObject->getTransactionLocationType() : ' ', $ApplyStyle),
                )
            )
            ),
            'fields' => '*'
        )
    );
    /*return array(
        'updateCells' => array(
            'start' => array(
                'sheetId' => $SheetId,
                'rowIndex' => $i,
                'columnIndex' => 0
            ),
            'rows' => array(array(
                'values' => array(
                    getCellValues(trim($DataObject->getId(), '{-}'), $ApplyStyle),
					getCellValues($DataObject->getDocNumber() ? $DataObject->getDocNumber() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getTxnDate() ? trim($DataObject->getTxnDate(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getDepartmentRef() ? $DataObject->getDepartmentRef() : ' ', $ApplyStyle),
					getCellValues($DataObject->getCurrencyRef() ? $DataObject->getCurrencyRef() : ' ', $ApplyStyle),
					getCellValues($DataObject->getExchangeRate() ? $DataObject->getExchangeRate() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getPrivateNote() ? trim($DataObject->getPrivateNote(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getLinkedTxn() ? $DataObject->getLinkedTxn() : ' ', $ApplyStyle),
					getCellValues($DataObject->getTxnTaxDetail() ? $DataObject->getTxnTaxDetail() : ' ', $ApplyStyle),
					getCellValues($DataObject->getVendorRef() ? trim($DataObject->getVendorRef(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getAPAccountRef() ? trim($DataObject->getAPAccountRef(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getTotalAmt() ? $DataObject->getTotalAmt() : ' ', $ApplyStyle),
					getCellValues($DataObject->getGlobalTaxCalculation() ? $DataObject->getGlobalTaxCalculation() : ' ', $ApplyStyle),
					getCellValues($DataObject->getSalesTermRef() ? $DataObject->getSalesTermRef() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getDueDate() ? $DataObject->getDueDate() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getBalance() ? $DataObject->getBalance() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getHomeBalance() ? $DataObject->getHomeBalance() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getTransactionLocationType() ? $DataObject->getTransactionLocationType() : ' ', $ApplyStyle),
                )
            )
            ),
            'fields' => '*'
        )
    );*/
}
function updateBillPaymentSheetRequest($DataObject, $i, $SheetId = 0, $ApplyStyle = false)
{return array(
        'updateCells' => array(
            'start' => array(
                'sheetId' => $SheetId,
                'rowIndex' => $i,
                'columnIndex' => 0
            ),
            'rows' => array(array(
                'values' => array(
                    getCellValues(trim($DataObject->getId(), '{-}'), $ApplyStyle),
					getCellValues($DataObject->getAttachableRef() ? $DataObject->getAttachableRef() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getFileName() ? trim($DataObject->getFileName(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getNote() ? $DataObject->getNotes() : ' ', $ApplyStyle),
					getCellValues($DataObject->getFileAccessUri() ? $DataObject->getFileAccessUri() : ' ', $ApplyStyle),
					getCellValues($DataObject->getTempDownloadUri() ? $DataObject->getTempDownloadUri() : ' ', $ApplyStyle),
					getCellValues($DataObject->getSize() ? $DataObject->getSize() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getContentType() ? trim($DataObject->getContentType(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getCategory() ? $DataObject->getCategory() : ' ', $ApplyStyle),
					getCellValues($DataObject->getLat() ? $DataObject->getLat() : ' ', $ApplyStyle),
					getCellValues($DataObject->getLong() ? trim($DataObject->getLong(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getPlaceName() ? trim($DataObject->getPlaceName(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getTag() ? $DataObject->getTag() : ' ', $ApplyStyle),
					getCellValues($DataObject->getThumbnailFileAccessUri() ? $DataObject->getThumbnailFileAccessUri() : ' ', $ApplyStyle),
					getCellValues($DataObject->getThumbnailTempDownloadUri() ? $DataObject->getThumbnailTempDownloadUri() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getAccountAlias() ? $DataObject->getAccountAlias() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getTxnLocationType() ? $DataObject->getTxnLocationType() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getTransactionLocationType() ? $DataObject->getTransactionLocationType() : ' ', $ApplyStyle),
                )
            )
            ),
            'fields' => '*'
        )
    );
    /*return array(
        'updateCells' => array(
            'start' => array(
                'sheetId' => $SheetId,
                'rowIndex' => $i,
                'columnIndex' => 0
            ),
            'rows' => array(array(
                'values' => array(
                    getCellValues(trim($DataObject->getId(), '{-}'), $ApplyStyle),
					getCellValues($DataObject->getDocNumber() ? $DataObject->getDocNumber() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getTxnDate() ? trim($DataObject->getTxnDate(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getDepartmentRef() ? $DataObject->getDepartmentRef() : ' ', $ApplyStyle),
					getCellValues($DataObject->getCurrencyRef() ? $DataObject->getCurrencyRef() : ' ', $ApplyStyle),
					getCellValues($DataObject->getExchangeRate() ? $DataObject->getExchangeRate() : ' ', $ApplyStyle),
					getCellValues($DataObject->getPrivateNote() ? $DataObject->getPrivateNote() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getLine() ? trim($DataObject->getLine(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getVendorRef() ? $DataObject->getVendorRef() : ' ', $ApplyStyle),
					getCellValues($DataObject->getAPAccountRef() ? $DataObject->getAPAccountRef() : ' ', $ApplyStyle),
					getCellValues($DataObject->getPayType() ? trim($DataObject->getPayType(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getCheckPayment() ? trim($DataObject->getCheckPayment(), '{-}') : ' ', $ApplyStyle),
					getCellValues($DataObject->getCreditCardPayment() ? $DataObject->getCreditCardPayment() : ' ', $ApplyStyle),
					getCellValues($DataObject->getTotalAmt() ? $DataObject->getTotalAmt() : ' ', $ApplyStyle),
					getCellValues($DataObject->getProcessBillPayment() ? $DataObject->getProcessBillPayment() : ' ', $ApplyStyle),
                    getCellValues($DataObject->getTransactionLocationType() ? $DataObject->getTransactionLocationType() : ' ', $ApplyStyle),
                )
            )
            ),
            'fields' => '*'
        )
    );*/
}
