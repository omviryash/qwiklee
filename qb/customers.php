<?php
ini_set('max_execution_time',3000);
require_once dirname(__FILE__) . '/../qwiklee_config.php';
require_once __DIR__ . '/../vendor/autoload.php';
if(isset($_SESSION['access_token']) && $_SESSION['access_token']) {
    define('SCOPES', implode(' ', array(
            Google_Service_Sheets::SPREADSHEETS_READONLY)
    ));
    $client = new Google_Client();
    $client->setScopes(SCOPES);
    $client->setAuthConfigFile('client_secret.json');
    $client->setAccessToken($_SESSION['access_token']);
    $service = new Google_Service_Sheets($client);
    $spreadsheetId = $_SESSION['google_sheet_id'];
    $SheetId = $_SESSION['customers_gid'];
    $range = "Customers!A:S";
    $response = $service->spreadsheets_values->get($spreadsheetId,$range);
    $values = $response->getValues();
    echo "<pre>";
    if (count($values) == 0) {
        $_SESSION['message'] = 'Something wrong try again!';
        $redirect_uri = PUBLIC_URL.'qb/customers_view.php';
        echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
    } else {
        $header = $values[0];
        unset($values[0]);
        $j = 0;
        $GsData = array();
        $GsDispName = array();
        foreach($values as $ExcelRow){
            $i = 0;
            foreach($ExcelRow as $ExcelCellValue){
                if(isset($header[$i])){
                    $GsData[$j][$header[$i]] = $ExcelCellValue;
                }
                if($header[$i] == 'display_name_as'){
                    $GsDispName[] = trim($ExcelCellValue);
                }
                $i++;
            }
            $j++;
        }

        $CustomerService = new QuickBooks_IPP_Service_Customer();
        $QbCustomerDataList = $CustomerService->query($Context,$realm,"SELECT * FROM Customer");
        $QbUserId = array();
        $QbDispName = array();
        $QBDataList = array();
        if(is_array($QbCustomerDataList)){
            foreach($QbCustomerDataList as $QbCustRow){
                $QbUserId[] = trim($QbCustRow->getId(),'{}-');
                $QbDispName[] = $QbCustRow->getDisplayName();
                $QBDataList[trim($QbCustRow->getId(),'{-}')] = array(
                    'user_id' => trim($QbCustRow->getId(),'{-}'),
                    'title' => $QbCustRow->getTitle()?trim($QbCustRow->getTitle()):' ',
                    'firstname' => $QbCustRow->getGivenName()?trim($QbCustRow->getGivenName()):' ',
                    'middlename' => $QbCustRow->getMiddleName()?trim($QbCustRow->getMiddleName()):' ',
                    'lastname' => $QbCustRow->getFamilyName()?trim($QbCustRow->getFamilyName()):' ',
                    'suffix' => $QbCustRow->getSuffix()?trim($QbCustRow->getSuffix()):' ',
                    'display_name_as' => $QbCustRow->getDisplayName()?trim($QbCustRow->getDisplayName()):' ',
                    'email' => $QbCustRow->getPrimaryEmailAddr()?trim($QbCustRow->getPrimaryEmailAddr()->getAddress()):' ',
                    'phone' => $QbCustRow->getPrimaryPhone()?trim($QbCustRow->getPrimaryPhone()->getFreeFormNumber()):' ',
                    'mobile' => $QbCustRow->getMobile()?trim($QbCustRow->getMobile()->getFreeFormNumber()):' ',
                    'address_line_1' => $QbCustRow->getBillAddr()?trim($QbCustRow->getBillAddr()->getLine1()):' ',
                    'address_line_2' => $QbCustRow->getBillAddr()?trim($QbCustRow->getBillAddr()->getLine2()):' ',
                    'city' => $QbCustRow->getBillAddr()?trim($QbCustRow->getBillAddr()->getCity()):' ',
                    'country_code' => $QbCustRow->getBillAddr() && $QbCustRow->getBillAddr()->getCountrySubDivisionCode()?trim($QbCustRow->getBillAddr()->getCountrySubDivisionCode()):' ',
                    'postal_code' => $QbCustRow->getBillAddr() && $QbCustRow->getBillAddr()->getPostalCode()?trim($QbCustRow->getBillAddr()->getPostalCode()):' ',
                );
            }
        }

        $GsRowIndex = 1;
        foreach($GsData as $GsRow) {
            if(isset($GsRow['display_name_as'])){
                // Create if record is new
                if(!in_array($GsRow['display_name_as'],$QbDispName)){
                    createCustomer($GsRow,$Context,$realm);
                }

                if(in_array($GsRow['display_name_as'],$QbDispName)){
                    //Check Record is update or not
                    if(isset($QBDataList[$GsRow['user_id']])){
                        if(!empty(array_diff($GsRow,$QBDataList[$GsRow['user_id']]))){
                            updateCustomer($QbCustomerDataList,$GsRow,$GsRow['display_name_as'],$Context,$realm);
                        }
                    }
                }
            }
            $GsRowIndex++;
        }
        foreach($QbDispName as $DispName){
            if(!in_array($DispName,$GsDispName)){
                deleteCustomer($QbCustomerDataList,$DispName,$Context,$realm);
            }
        }
    }

    $CustomerService = new QuickBooks_IPP_Service_Vendor();
    $QbCustomerDataList = $CustomerService->query($Context,$realm,"SELECT Id FROM Customer ORDER BY Id");
    $QbUserId = array();
    if(is_array($QbCustomerDataList)){
        foreach($QbCustomerDataList as $QbCustRow){
            $QbUserId[] = trim($QbCustRow->getId(),'{}-');
        }
    }

    $range = "Customers!A2:A";
    $valueRange= new Google_Service_Sheets_ValueRange();
    $valueRange->setValues(["values"=>$QbUserId]);
    $valueRange->setMajorDimension('COLUMNS');
    $conf = ["valueInputOption" => "RAW"];
    $service->spreadsheets_values->update($spreadsheetId,$range,$valueRange,$conf);
    $_SESSION['message'] = 'Google Sheet To QuickBooks Data Successfully Saved!';
    $redirect_uri = BASEURL.'qb/customers_view.php';
    echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
} else {
    $redirect_uri = BASEURL.'logout.php';
    echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
}

/**
 * @param $CustomerData
 * @param $Context
 * @param $realm
 * @return int
 */
function createCustomer($CustomerData,$Context,$realm){
    $Customer = new QuickBooks_IPP_Object_Customer();
    $Customer->setTitle(isset($CustomerData['title'])?$CustomerData['title']:'');
    $Customer->setGivenName(isset($CustomerData['firstname'])?$CustomerData['firstname']:'');
    $Customer->setMiddleName(isset($CustomerData['middlename'])?$CustomerData['middlename']:'');
    $Customer->setFamilyName(isset($CustomerData['lastname'])?$CustomerData['lastname']:'');
    $Customer->setSuffix(isset($CustomerData['suffix'])?$CustomerData['suffix']:'');
    $Customer->setDisplayName(isset($CustomerData['display_name_as'])?$CustomerData['display_name_as']:'');
    $Customer->setSalesTermRef(4);

    /*------ Email ----------*/
    $PrimaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
    $PrimaryEmailAddr->setAddress(isset($CustomerData['email'])?$CustomerData['email']:'');
    $Customer->setPrimaryEmailAddr($PrimaryEmailAddr);

    /*------ Phone ----------*/
    $PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
    $PrimaryPhone->setFreeFormNumber(isset($CustomerData['phone'])?$CustomerData['phone']:'');
    $Customer->setPrimaryPhone($PrimaryPhone);

    // Mobile #
    $Mobile = new QuickBooks_IPP_Object_Mobile();
    $Mobile->setFreeFormNumber(isset($CustomerData['mobile'])?$CustomerData['mobile']:'');
    $Customer->setMobile($Mobile);

    // Bill address
    $BillAddr = new QuickBooks_IPP_Object_BillAddr();
    $BillAddr->setLine1(isset($CustomerData['address_line_1'])?$CustomerData['address_line_1']:'');
    $BillAddr->setLine2(isset($CustomerData['address_line_2'])?$CustomerData['address_line_2']:'');
    $BillAddr->setCity(isset($CustomerData['city'])?$CustomerData['city']:'');
    $BillAddr->setCountrySubDivisionCode(isset($CustomerData['country_code'])?$CustomerData['country_code']:'');
    $BillAddr->setPostalCode(isset($CustomerData['postal_code'])?$CustomerData['postal_code']:'');
    $Customer->setBillAddr($BillAddr);
    $CustomerService = new QuickBooks_IPP_Service_Customer();
    return $CustomerService->add($Context,$realm,$Customer);
    /*print_r($Customer);
    $CustomerService->add($Context,$realm,$Customer);
    print($CustomerService->lastError($Context));
    die;*/
}

/**
 * @param $CustomerData
 * @param $DispName
 * @param $Context
 * @param $realm
 * @return bool
 */
function updateCustomer($DataList,$CustomerData,$DispName,$Context,$realm){
    $Customer = getCustObjByCustDisplayName($DataList,$DispName);;
    $Customer->setTitle(isset($CustomerData['title'])?$CustomerData['title']:' ');
    $Customer->setGivenName(isset($CustomerData['firstname'])?$CustomerData['firstname']:' ');
    $Customer->setMiddleName(isset($CustomerData['middlename'])?$CustomerData['middlename']:' ');
    $Customer->setFamilyName(isset($CustomerData['lastname'])?$CustomerData['lastname']:' ');
    $Customer->setSuffix(isset($CustomerData['suffix'])?$CustomerData['suffix']:' ');
    $Customer->setDisplayName(isset($CustomerData['display_name_as'])?$CustomerData['display_name_as']:' ');
    $Customer->setSalesTermRef(4);

    /*------ Email ----------*/
    $PrimaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
    $PrimaryEmailAddr->setAddress(isset($CustomerData['email'])?$CustomerData['email']:' ');
    $Customer->setPrimaryEmailAddr($PrimaryEmailAddr);

    /*------ Phone ----------*/
    $PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
    $PrimaryPhone->setFreeFormNumber(isset($CustomerData['phone'])?$CustomerData['phone']:' ');
    $Customer->setPrimaryPhone($PrimaryPhone);

    /*------ Mobile ----------*/
    $Mobile = new QuickBooks_IPP_Object_Mobile();
    $Mobile->setFreeFormNumber(isset($CustomerData['mobile'])?$CustomerData['mobile']:' ');
    $Customer->setMobile($Mobile);

    /*------ Bill address ----------*/
    $BillAddr = new QuickBooks_IPP_Object_BillAddr();
    $BillAddr->setLine1(isset($CustomerData['address_line_1'])?$CustomerData['address_line_1']:' ');
    $BillAddr->setLine2(isset($CustomerData['address_line_2'])?$CustomerData['address_line_2']:' ');
    $BillAddr->setCity(isset($CustomerData['city'])?$CustomerData['city']:'');
    $BillAddr->setCountrySubDivisionCode(isset($CustomerData['country_code'])?$CustomerData['country_code']:' ');
    $BillAddr->setPostalCode(isset($CustomerData['postal_code'])?$CustomerData['postal_code']:' ');
    $Customer->setBillAddr($BillAddr);
    $CustomerService = new QuickBooks_IPP_Service_Customer();
    return $CustomerService->update($Context, $realm,$Customer->getId(),$Customer);
}

/**
 * @param $DispName
 * @param $Context
 * @param $realm
 * @return bool
 */
function deleteCustomer($DataList,$DispName,$Context,$realm){
    $Customer = getCustObjByCustDisplayName($DataList,$DispName);
    $Customer->setActive('false');
    $CustomerService = new QuickBooks_IPP_Service_Customer();
    return $CustomerService->update($Context,$realm,$Customer->getId(),$Customer);

}

/**
 * @param $DataList
 * @param $DisplayName
 * @return mixed
 */
function getCustObjByCustDisplayName($DataList,$DisplayName){
    for($i=0;$i<count($DataList);$i++){
        if($DataList[$i]->getDisplayName() == $DisplayName){
            return $DataList[$i];
        }
    }
}