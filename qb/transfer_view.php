<?php
require_once dirname(__FILE__) . '/../header.php';
require_once dirname(__FILE__) . '/../sidebar.php';
require_once __DIR__ . '/../vendor/autoload.php';
define('SCOPES', implode(' ', array(
        Google_Service_Sheets::SPREADSHEETS)
));
$client = new Google_Client();
$client->setScopes(SCOPES);
$client->setAuthConfigFile('client_secret.json');
$google_sheet_id = $_SESSION['google_sheet_id'];
?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header" style="padding: 15px!important;">
            <h1 style="display:inline-block;">
                Google Sheet To QuickBooks
            </h1>
            <a href="<?=BASEURL?>qb/transfer.php" class="btn btn-success btn-flat pull-right">Save GS To QB</a>
        </section>
        <?php
            if(isset($_SESSION['message']) && $_SESSION['message'] != ''){
        ?>
                <div class="callout callout-success">
                    <h4><?=$_SESSION['message']?></h4>
                </div>
        <?php
                $_SESSION['message'] = '';
                unset($_SESSION['message']);
            }
        ?>
        <iframe height="750" style="width:100%" src="https://docs.google.com/spreadsheets/d/<?=$google_sheet_id;?>/edit?rm=minimal#gid=<?=$_SESSION['transfer_gid'];?>"></iframe>
    </div>
    <!-- /.content-wrapper -->
<?php
require_once dirname(__FILE__) . '/../footer.php';
?>
