<?php
session_start();
require 'lightopenid-lightopenid/openid.php';
try {
    $openid = new LightOpenID('localhost');
    if (!$openid->mode){
        $quickbooks_is_connected = false;
        $_SESSION['is_qb_login'] = false;
        $openid->identity = 'https://openid.intuit.com/openid/xrds';
        $openid->required = array('contact/email');
        $openid->optional = array('namePerson');
        header('Location: ' . $openid->authUrl());
    }else if ($openid->mode == 'cancel'){
        $quickbooks_is_connected = false;
        $_SESSION['is_qb_login'] = false;
    }else {
        if ($openid->validate()) {
            $quickbooks_is_connected = true;
            $_SESSION['is_qb_login'] = true;
            $identity = $openid->identity;
            $attr = $openid->getAttributes();
            $email = $attr['contact/email'];
            $name = $attr['namePerson'];
            print_r($openid);
            die;
        } else {
            $quickbooks_is_connected = false;
            $_SESSION['is_qb_login'] = false;
            die('Declined login?');
        }

    }
}catch(ErrorException $e){
    die('Sorry: ' . $e->getMessage());
}
