<?php

require_once dirname(__FILE__) . '/../header.php';
require_once dirname(__FILE__) . '/../sidebar.php';
require_once __DIR__ . '/../vendor/autoload.php';
define('SCOPES', implode(' ', array(
        Google_Service_Sheets::SPREADSHEETS)
));
$client = new Google_Client();
$client->setScopes(SCOPES);
$client->setAuthConfigFile('client_secret.json');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Sheets($client);
            $spreadsheetId = google_sheet_id;
            $requests = array();
            /**
             * Fetch User id from Google Sheet
             */
            $SheetArray = array('Customers','Vendors','Invoices','Sales Receipts');
            for($k=0;$k<4;$k++){
                $SheetName = $SheetArray[$k];
                if($k == 0){
                    $SheetId = $_SESSION['customers_gid'];
                    $DataObjectList = getDataList($Context,$realm,'Customer');
                }elseif($k == 1){
                    $SheetId = $_SESSION['vendors_gid'];
                    $DataObjectList = getDataList($Context,$realm,'Vendor');
                }elseif($k == 2){
                    $SheetId = $_SESSION['invoices_gid'];
                    $DataObjectList = getDataList($Context,$realm,'Invoice');
                }elseif($k == 3){
                    $SheetId = $_SESSION['sales_receipts_gid'];
                    $DataObjectList = getDataList($Context,$realm,'SalesReceipt');
                }else{
                    $SheetId = 0;
                    $DataObjectList = getDataList($Context,$realm,'Customer');
                }
                /**
                 * Fetch User id from Google Sheet
                 */
                $range = "$SheetName!A:A";
                $response = $service->spreadsheets_values->get($spreadsheetId,$range);
                $values = $response->getValues();
                if (count($values) == 0) {
                } else {
                    $header = $values[0];
                    unset($values[0]);
                    $UserIds = '';
                    $i = 0;
                    foreach ($values as $ExcelRow) {
                        foreach ($ExcelRow as $ExcelCellValue) {
                            if($ExcelCellValue != '' && is_numeric($ExcelCellValue)){
                                if($i != 0){
                                    $UserIds .=",";
                                }
                                $UserIds .= $ExcelCellValue;
                                $i++;
                            }
                        }
                    }
                }
                $UserIdsArr = explode(',',$UserIds);
                /**
                 * /Fetch User id from Google Sheet
                 */
                $user_ids = '';
                if(is_array($DataObjectList) && !empty($DataObjectList)) {
                    $i = 1;
                    foreach ($DataObjectList as $DataObject) {
                        $user_id = trim($DataObject->getId(),'{-}');
                        if(in_array($user_id,$UserIdsArr)){
                            $ApplyStyle = false;
                        }else{
                            $ApplyStyle = true;
                        }
                        if($ApplyStyle){
                            $requests[] = new Google_Service_Sheets_Request(array(
                                'updateDimensionProperties' => array(
                                    'range' => array(
                                        'sheetId' => $SheetId,
                                        "dimension" => "ROWS",
                                        'startIndex' => $i,
                                        'endIndex' => $i+1,
                                    ),
                                    "properties" => array("pixelSize" =>60),
                                    'fields' => 'pixelSize'
                                )
                            ));
                        }else{
                            $requests[] = new Google_Service_Sheets_Request(array(
                                'updateDimensionProperties' => array(
                                    'range' => array(
                                        'sheetId' => $SheetId,
                                        "dimension" => "ROWS",
                                        'startIndex' => $i,
                                        'endIndex' => $i+1,
                                    ),
                                    "properties" => array("pixelSize" =>20),
                                    'fields' => 'pixelSize'
                                )
                            ));
                        }
                        if($k == 0){
                            $requests[] = new Google_Service_Sheets_Request(updateCustomersSheetRequest($DataObject,$i,$SheetId,$ApplyStyle));
                        }elseif($k == 1){
                            $requests[] = new Google_Service_Sheets_Request(updateVendorsSheetRequest($DataObject,$i,$SheetId,$ApplyStyle));
                        }elseif($k == 2){
                            $requests[] = new Google_Service_Sheets_Request(updateInvoicesSheetRequest($DataObject,$i,$SheetId,$ApplyStyle));
                        }elseif($k == 3){
                            $requests[] = new Google_Service_Sheets_Request(updateSalesReceiptsSheetRequest($DataObject,$i,$SheetId,$ApplyStyle));
                        }else{
                            $requests[] = new Google_Service_Sheets_Request(updateCustomersSheetRequest($DataObject,$i,$SheetId,$ApplyStyle));
                        }
                        $i++;
                    }

                    $requests[] = new Google_Service_Sheets_Request(array(
                        "deleteDimension" => array(
                            "range" => array(
                                'sheetId' => $SheetId,
                                'dimension' => 'ROWS',
                                'startIndex' => $i+1
                            )
                        )
                    ));

                    $requests[] = new Google_Service_Sheets_Request(array(
                        "insertDimension" => array(
                            "range" => array(
                                'sheetId' => $SheetId,
                                'dimension' => 'ROWS',
                                'startIndex' => $i,
                                'endIndex' => 1000
                            )
                        )
                    ));
                }
                $batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest(array(
                    'requests' => $requests
                ));
                $service->spreadsheets->batchUpdate($spreadsheetId,$batchUpdateRequest);
            }
            $redirect_uri = BASEURL.'qb/qb_to_gs_view.php';
            echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
        } else {
            $redirect_uri = BASEURL.'logout.php';
            echo '<script>window.location.replace("'.$redirect_uri.'");</script>';
        }
    ?>

</div>
<!-- /.content-wrapper -->
<?php
require_once dirname(__FILE__) . '/../footer.php';

/**
 * @param $fieldValue
 * @param bool $styleApply
 * @return array
 */
function getCellValues($fieldValue,$styleApply = true){
    if($styleApply){
        return array(
            'userEnteredValue' => array('stringValue' => $fieldValue),
            'userEnteredFormat' => array(
                'backgroundColor' => array('red' => 0.3,'blue' => 0.3,'green' => 0.3),
                'horizontalAlignment' => 'LEFT',
                'textFormat' => array(
                    'foregroundColor' => array('red' => 1,'blue' => 1,'green' => 1),
                ),
                'borders' => array(
                    'top' => array('style'=>'SOLID','width'=>3,'color' => array('red' =>0.01,'blue' => 0.01,'green' => 0.01)),
                    'right' => array('style'=>'SOLID','width'=>3,'color' => array('red' =>0.01,'blue' =>0.01,'green' => 0.01)),
                    'bottom' => array('style'=>'SOLID','width'=>3,'color' => array('red' => 0.01,'blue' =>0.01,'green' => 0.01)),
                    'left' => array('style'=>'SOLID','width'=>3,'color' => array('red' => 0.01,'blue' =>0.01,'green' => 0.01))
                )
            )
        );
    }else{
        return array('userEnteredValue' => array('stringValue' => $fieldValue));
    }
}
function getRowStyle($styleApply = true,$SheetId = 0){
    if($styleApply){
        return array(
            'range' => array(
                'sheetId' => 0,
                "dimension" => "ROWS",
                'startIndex' => $SheetId,
            ),
            "properties" => array("pixelSize" =>60),
            'fields' => 'pixelSize'
        );
    }else{
        return array(
            'range' => array(
                'sheetId' => $SheetId,
                "dimension" => "ROWS",
                'startIndex' => 0,
            ),
            "properties" => array("pixelSize" =>20),
            'fields' => 'pixelSize'
        );
    }
}

function getDataList($Context,$realm,$table_name){
    $CustomerService = new QuickBooks_IPP_Service_Customer();
    $DataObjectList = $CustomerService->query($Context,$realm,"SELECT * FROM $table_name ORDER BY Id");
    return $DataObjectList;
}

function updateCustomersSheetRequest($DataObject,$i,$SheetId = 0,$ApplyStyle = false){
    return array(
        'updateCells' => array(
            'start' => array(
                'sheetId' => $SheetId,
                'rowIndex' => $i,
                'columnIndex' => 0
            ),
            'rows' => array(array(
                'values' => array(
                    getCellValues(trim($DataObject->getId(),'{-}'),$ApplyStyle),
                    getCellValues($DataObject->getTitle()?$DataObject->getTitle():' ',$ApplyStyle),
                    getCellValues($DataObject->getGivenName()?$DataObject->getGivenName():' ',$ApplyStyle),
                    getCellValues($DataObject->getMiddleName()?$DataObject->getMiddleName():' ',$ApplyStyle),
                    getCellValues($DataObject->getFamilyName()?$DataObject->getFamilyName():' ',$ApplyStyle),
                    getCellValues($DataObject->getSuffix()?$DataObject->getSuffix():' ',$ApplyStyle),
                    getCellValues($DataObject->getDisplayName()?$DataObject->getDisplayName():' ',$ApplyStyle),
                    getCellValues($DataObject->getPrimaryEmailAddr()?$DataObject->getPrimaryEmailAddr()->getAddress():' ',$ApplyStyle),
                    getCellValues($DataObject->getPrimaryPhone()?$DataObject->getPrimaryPhone()->getFreeFormNumber():' ',$ApplyStyle),
                    getCellValues($DataObject->getMobile()?$DataObject->getPrimaryPhone()->getFreeFormNumber():' ',$ApplyStyle),
                    getCellValues($DataObject->getBillAddr()?$DataObject->getBillAddr()->getLine1():' ',$ApplyStyle),
                    getCellValues($DataObject->getBillAddr() && $DataObject->getBillAddr()->getLine2()?$DataObject->getBillAddr()->getLine2():' ',$ApplyStyle),
                    getCellValues($DataObject->getBillAddr()?$DataObject->getBillAddr()->getCity():' ',$ApplyStyle),
                    getCellValues($DataObject->getBillAddr() && $DataObject->getBillAddr()->getCountrySubDivisionCode()?$DataObject->getBillAddr()->getCountrySubDivisionCode():' ',$ApplyStyle),
                    getCellValues($DataObject->getBillAddr() && $DataObject->getBillAddr()->getPostalCode()?$DataObject->getBillAddr()->getPostalCode():' ',$ApplyStyle),
                )
            )
            ),
            'fields' => '*'
        )
    );
}

function updateVendorsSheetRequest($DataObject,$i,$SheetId = 0,$ApplyStyle = false){
    return array(
        'updateCells' => array(
            'start' => array(
                'sheetId' => $SheetId,
                'rowIndex' => $i,
                'columnIndex' => 0
            ),
            'rows' => array(array(
                'values' => array(
                    getCellValues(trim($DataObject->getId(),'{-}'),$ApplyStyle),
                    getCellValues($DataObject->getTitle()?$DataObject->getTitle():' ',$ApplyStyle),
                    getCellValues($DataObject->getGivenName()?$DataObject->getGivenName():' ',$ApplyStyle),
                    getCellValues($DataObject->getMiddleName()?$DataObject->getMiddleName():' ',$ApplyStyle),
                    getCellValues($DataObject->getFamilyName()?$DataObject->getFamilyName():' ',$ApplyStyle),
                    getCellValues($DataObject->getSuffix()?$DataObject->getSuffix():' ',$ApplyStyle),
                    getCellValues($DataObject->getDisplayName()?$DataObject->getDisplayName():' ',$ApplyStyle),
                    getCellValues($DataObject->getPrimaryEmailAddr()?$DataObject->getPrimaryEmailAddr()->getAddress():' ',$ApplyStyle),
                    getCellValues($DataObject->getPrimaryPhone()?$DataObject->getPrimaryPhone()->getFreeFormNumber():' ',$ApplyStyle),
                    getCellValues($DataObject->getMobile()?$DataObject->getPrimaryPhone()->getFreeFormNumber():' ',$ApplyStyle),
                    getCellValues($DataObject->getBillAddr()?$DataObject->getBillAddr()->getLine1():' ',$ApplyStyle),
                    getCellValues($DataObject->getBillAddr() && $DataObject->getBillAddr()->getLine2()?$DataObject->getBillAddr()->getLine2():' ',$ApplyStyle),
                    getCellValues($DataObject->getBillAddr()?$DataObject->getBillAddr()->getCity():' ',$ApplyStyle),
                    getCellValues($DataObject->getBillAddr() && $DataObject->getBillAddr()->getCountrySubDivisionCode()?$DataObject->getBillAddr()->getCountrySubDivisionCode():' ',$ApplyStyle),
                    getCellValues($DataObject->getBillAddr() && $DataObject->getBillAddr()->getPostalCode()?$DataObject->getBillAddr()->getPostalCode():' ',$ApplyStyle),
                )
            )
            ),
            'fields' => '*'
        )
    );
}

function updateInvoicesSheetRequest($DataObject,$i,$SheetId = 0,$ApplyStyle = false){
    return array(
        'updateCells' => array(
            'start' => array(
                'sheetId' => $SheetId,
                'rowIndex' => $i,
                'columnIndex' => 0
            ),
            'rows' => array(array(
                'values' => array(
                    getCellValues(trim($DataObject->getId(),'{-}'),$ApplyStyle),
                    getCellValues($DataObject->getDocNumber()?$DataObject->getDocNumber():' ',$ApplyStyle),
                    getCellValues($DataObject->getCustomerRef()?trim($DataObject->getCustomerRef(),'{-}'):' ',$ApplyStyle),
                    getCellValues($DataObject->getCustomerRef_name()?$DataObject->getCustomerRef_name():' ',$ApplyStyle),
                    getCellValues($DataObject->getTotalAmt()?$DataObject->getTotalAmt():' ',$ApplyStyle),
                    getCellValues($DataObject->getCurrencyRef()?trim($DataObject->getCurrencyRef(),'{-}'):' ',$ApplyStyle),
                    getCellValues($DataObject->getDueDate()?$DataObject->getDueDate():' ',$ApplyStyle),
                )
            )
            ),
            'fields' => '*'
        )
    );
}

function updateSalesReceiptsSheetRequest($DataObject,$i,$SheetId = 0,$ApplyStyle = false){
    return array(
        'updateCells' => array(
            'start' => array(
                'sheetId' => $SheetId,
                'rowIndex' => $i,
                'columnIndex' => 0
            ),
            'rows' => array(array(
                'values' => array(
                    getCellValues(trim($DataObject->getId(),'{-}'),$ApplyStyle),
                    getCellValues($DataObject->getDocNumber()?$DataObject->getDocNumber():' ',$ApplyStyle),
                    getCellValues($DataObject->getCustomerRef()?trim($DataObject->getCustomerRef(),'{-}'):' ',$ApplyStyle),
                    getCellValues($DataObject->getCustomerRef_name()?$DataObject->getCustomerRef_name():' ',$ApplyStyle),
                    getCellValues($DataObject->getTotalAmt()?$DataObject->getTotalAmt():' ',$ApplyStyle),
                    getCellValues($DataObject->getCurrencyRef()?trim($DataObject->getCurrencyRef(),'{-}'):' ',$ApplyStyle),
                    getCellValues($DataObject->getPaymentMethodRef_name()?$DataObject->getPaymentMethodRef_name():' ',$ApplyStyle),
                )
            )
            ),
            'fields' => '*'
        )
    );
}