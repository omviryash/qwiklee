<?php
include 'qwiklee_config.php';
require_once __DIR__ . '/vendor/autoload.php';
define('SCOPES', implode(' ', array(
        Google_Service_Sheets::SPREADSHEETS)
));
$client = new Google_Client();
$client->setScopes(SCOPES);
$client->setAuthConfigFile('client_secret.json');
if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
    $client->setAccessToken($_SESSION['access_token']);
    $service = new Google_Service_Sheets($client);
    /*----------- Create Google SpreadSheet ---------------*/
    $CreateSprSheetRequest = new Google_Service_Sheets_Spreadsheet();
    $GSProperty = new Google_Service_Sheets_SpreadsheetProperties();
    $GSProperty->setTitle('GoogleSheet Qwiklee');
    $CreateSprSheetRequest->setProperties($GSProperty);
    $response = $service->spreadsheets->create($CreateSprSheetRequest);
    $spreadsheetId = $response->getSpreadsheetId();
    /*----------- Create Google SpreadSheet ---------------*/

    $CopySheetRequest = new Google_Service_Sheets_CopySheetToAnotherSpreadsheetRequest();
    $CopySheetRequest->setDestinationSpreadsheetId($spreadsheetId);
   
    $MainSheetId = array('1973597300','601907652','832169343','845709544','334452364','1236916780','0','1792360675','1171204834',
                          '375578738','979499298','89323535','1091069183','438259888','455164403','1938888972','731619096','2060104378','2021497749',
                           '1591246989','1911885398','333687254','1563317784','1611434985','2067977339','303571956');
    $MainSheetTitle = array('Account','Attachable','Bill','BillPayment','Class','CreditMemo','Customers','Department','Deposit',
                             'Employee','Estimate','Invoices','InvoiceUpdate','Item','JournalEntry','Payment','PaymentMethods','Purchase','PurchaseOrder',
                              'RefundReceipt','SalesReceipts','Term','TimeActivity','Transfer','Vendors','VendorCredit');
    $MainSpreadSheetId = '1Ensmtzv0dqIMg6VwSrtLlF0qk8gX0E1FPY7fRj2uoFQ';
    $CreatedSheetId = array();
    for($i=0;$i<26;$i++){
        $response = $service->spreadsheets_sheets->copyTo($MainSpreadSheetId,$MainSheetId[$i],$CopySheetRequest);
        $CreatedSheetId[$MainSheetTitle[$i]] = $response->getSheetId();
    }
    $requests = array();
    for($i=25;$i>=0;$i--){
        $requests[] = new Google_Service_Sheets_Request(array(
            'updateSheetProperties' => array(
                "properties" => array(
                    "sheetId" =>$CreatedSheetId[$MainSheetTitle[$i]],
                    "title" =>$MainSheetTitle[$i],
                    "gridProperties"=>array(
                            'rowCount'=>1000,
                            'columnCount'=>26,
                        )
                    ),
                'fields' => '*'
            )
        ));
    }
    $batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest(array(
        'requests' => $requests
    ));
    $response = $service->spreadsheets->batchUpdate($spreadsheetId,$batchUpdateRequest);

    /*------------------- Insert Google Sheet Data IN DB ----------------------------------------*/

    $user_id = $_SESSION['user_id'];
    $google_sheet_id = $spreadsheetId;
    $account_gid = $CreatedSheetId['Account'];
    $attachable_gid = $CreatedSheetId['Attachable'];
    $bill_gid = $CreatedSheetId['Bill'];
    $billpayment_gid = $CreatedSheetId['BillPayment'];
    $class_gid = $CreatedSheetId['Class'];
    $creditmemo_gid = $CreatedSheetId['CreditMemo'];
    $customers_gid = $CreatedSheetId['Customers'];
    $departmemt_gid = $CreatedSheetId['Department'];
    $deposit_gid = $CreatedSheetId['Deposit'];
    $employee_gid = $CreatedSheetId['Employee'];
    $estimate_gid = $CreatedSheetId['Estimate'];
    $invoices_gid = $CreatedSheetId['Invoices'];
    $invoice_update_gid = $CreatedSheetId['InvoiceUpdate'];
    $item_gid = $CreatedSheetId['Item'];
    $journalentry_gid = $CreatedSheetId['JournalEntry'];
    $payment_gid = $CreatedSheetId['Payment'];
    $paymentmethods_gid = $CreatedSheetId['PaymentMethods'];
    $purchase_gid = $CreatedSheetId['Purchase'];
	$purchaseorders_gid = $CreatedSheetId['PurchaseOrder'];
    $refundreceipt_gid = $CreatedSheetId['RefundReceipt'];
    $sales_receipts_gid = $CreatedSheetId['SalesReceipts'];
    $term_gid = $CreatedSheetId['Term'];
    $timeactivity_gid = $CreatedSheetId['TimeActivity'];
    $transfer_gid = $CreatedSheetId['Transfer'];
    $vendors_gid = $CreatedSheetId['Vendors'];
    $vendorcredit_gid = $CreatedSheetId['VendorCredit'];
    $update_query = "UPDATE `qwiklee_users` SET
                        `google_sheet_id`='$google_sheet_id',
                        `account_gid`='$account_gid',
                        `attachable_gid`='$attachable_gid',
                        `bill_gid`='$bill_gid',
                        `billpayment_gid`='$billpayment_gid',
                        `class_gid`='$class_gid',
                        `creditmemo_gid`='$creditmemo_gid',
                        `customers_gid`='$customers_gid',
                        `departmemt_gid`='$departmemt_gid',
                        `deposit_gid`='$deposit_gid',
                        `employee_gid`='$employee_gid',
                        `estimate_gid`='$estimate_gid',
                        `invoices_gid`='$invoices_gid',
                        `item_gid`='$item_gid',
                        `invoice_update_gid`='$invoice_update_gid',
                        `journalentry_gid`='$journalentry_gid',
                        `payment_gid`='$payment_gid',
                        `paymentmethods_gid`='$paymentmethods_gid',
                        `purchase_gid`='$purchase_gid',
                        `purchaseorders_gid`='$purchaseorders_gid',
                        `refundreceipt_gid`='$refundreceipt_gid',
                        `sales_receipts_gid`='$sales_receipts_gid',
                        `term_gid`='$term_gid',
                        `timeactivity_gid`='$timeactivity_gid',
                        `transfer_gid`='$transfer_gid',
                        `vendors_gid`='$vendors_gid',
                        `vendorcredit_gid`='$vendorcredit_gid'
                        WHERE `user_id` = '$user_id'";
                        
    mysqli_query($connection_obj,$update_query);
    $_SESSION['google_sheet_id'] = $google_sheet_id;
    $_SESSION['account_gid'] = $account_gid;
    $_SESSION['attachable_gid'] = $attachable_gid;
    $_SESSION['bill_gid'] = $bill_gid;
    $_SESSION['bill_payment_gid'] = $billpayment_gid;
    $_SESSION['class_gid'] = $class_gid;
    $_SESSION['creditmemo_gid'] = $creditmemo_gid;
    $_SESSION['customers_gid'] = $customers_gid;
    $_SESSION['departmemt_gid'] = $departmemt_gid;
    $_SESSION['deposit_gid'] = $deposit_gid;
    $_SESSION['employee_gid'] = $employee_gid;
    $_SESSION['estimate_gid'] = $estimate_gid;
    $_SESSION['invoices_gid'] = $invoices_gid;
    $_SESSION['item_gid'] = $item_gid;
    $_SESSION['invoice_update_gid'] = $invoice_update_gid;
    $_SESSION['journalentry_gid'] = $journalentry_gid;
    $_SESSION['payment_gid'] = $payment_gid;
    $_SESSION['paymentmethods_gid'] = $paymentmethods_gid;
    $_SESSION['purchase_gid'] = $purchase_gid;
    $_SESSION['refundreceipt_gid'] = $refundreceipt_gid;
    $_SESSION['sales_receipts_gid'] = $sales_receipts_gid;
    $_SESSION['term_gid'] = $term_gid;
    $_SESSION['timeactivity_gid'] = $timeactivity_gid;
    $_SESSION['transfer_gid'] = $transfer_gid;
    $_SESSION['vendors_gid'] = $vendors_gid;
    $_SESSION['vendorcredit_gid'] = $vendorcredit_gid;
    $redirect_uri = BASEURL . 'dashboard.php';
    echo '<script>window.location.replace("' . $redirect_uri . '");</script>';
    }else {																																																																																									
    $redirect_uri = BASEURL . 'logout.php';
    echo '<script>window.location.replace("' . $redirect_uri . '");</script>';
}
