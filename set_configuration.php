<?php
include 'header.php';
include 'sidebar.php';
    $user_id = $_SESSION['user_id'];
    $query = "SELECT * FROM  `qwiklee_users` WHERE `user_id` = '$user_id' LIMIT 1";
    $result = mysqli_query($connection_obj,$query);
    $UserRow = mysqli_fetch_object($result);
    $google_sheet_id = isset($_POST['google_sheet_id'])?trim($_POST['google_sheet_id']):trim($UserRow->google_sheet_id);
    $customers_gid = isset($_POST['customers_gid'])?trim($_POST['customers_gid']):trim($UserRow->customers_gid);
    $vendors_gid = isset($_POST['vendors_gid'])?trim($_POST['vendors_gid']):trim($UserRow->vendors_gid);
    $invoices_gid = isset($_POST['invoices_gid'])?trim($_POST['invoices_gid']):trim($UserRow->invoices_gid);
    $invoice_update_gid = isset($_POST['invoice_update_gid'])?trim($_POST['invoice_update_gid']):trim($UserRow->invoice_update_gid);
    $sales_receipts_gid = isset($_POST['sales_receipts_gid'])?trim($_POST['sales_receipts_gid']):trim($UserRow->sales_receipts_gid);
    $update_query = "UPDATE `qwiklee_users` SET
                        `google_sheet_id`='$google_sheet_id',
                        `customers_gid`='$customers_gid',
                        `vendors_gid`='$vendors_gid',
                        `invoices_gid`='$invoices_gid',
                        `invoice_update_gid`='$invoice_update_gid',
                        `sales_receipts_gid`='$sales_receipts_gid'
                        WHERE `user_id` = '$user_id'";
        mysqli_query($connection_obj,$update_query);
    if(isset($_POST['google_sheet_id']) && $_POST['google_sheet_id'] != ''){
        $_SESSION['google_sheet_id'] = trim($_POST['google_sheet_id']);
        $_SESSION['customers_gid'] = trim($_POST['customers_gid']);
        $_SESSION['vendors_gid'] = trim($_POST['vendors_gid']);
        $_SESSION['invoices_gid'] = trim($_POST['invoices_gid']);
        $_SESSION['invoice_update_gid'] = trim($_POST['invoice_update_gid']);
        $_SESSION['sales_receipts_gid'] = trim($_POST['sales_receipts_gid']);
        $message = 'Configuration Successfully Saved!';
    }else{
        $message = '';
    }
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php
                if($message != ''){
                    ?>
                    <div class="callout callout-success">
                        <h4><?=$message;?></h4>
                    </div>
                    <?php
                    $message = '';
                }
                ?>
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Set Configuration</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="" method="post">
                        <input type="hidden" name="user_id" value="<?=$user_id;?>">
                        <div class="box-body">
                            <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Sample Google Sheet</label>
                                    <div class="col-sm-9">
                                        <a href="https://docs.google.com/spreadsheets/d/1Ensmtzv0dqIMg6VwSrtLlF0qk8gX0E1FPY7fRj2uoFQ/edit?usp=sharing" target="_blank">https://docs.google.com/spreadsheets/d/1Ensmtzv0dqIMg6VwSrtLlF0qk8gX0E1FPY7fRj2uoFQ/edit?usp=sharing</a>
                                    </div>
                            </div>
                            <div class="clearfix"></div>
                            <br/>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Google Sheet Id</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="google_sheet_id" name="google_sheet_id" value="<?=$google_sheet_id;?>" placeholder="Google Sheet Id" required="required">
                                    <br/>
                                    Like, From : https://docs.google.com/spreadsheets/d/1mWLJ8Mq7KEWG8PoB59W5oZCjOslzBIN0aWSmsayYih0/edit#gid=0
                                    <br/>
                                    => 1mWLJ8Mq7KEWG8PoB59W5oZCjOslzBIN0aWSmsayYih0
                                    <br/>
                                    <br/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Customers gid</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="customers_gid" name="customers_gid" value="<?=$customers_gid;?>" placeholder="Customers gid" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Vendors gid</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="vendors_gid" name="vendors_gid" value="<?=$vendors_gid;?>" placeholder="Vendors gid" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Invoices gid</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="invoices_gid" name="invoices_gid" value="<?=$invoices_gid;?>" placeholder="Invoices gid" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Invoices Update gid</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="invoice_update_gid" name="invoice_update_gid" value="<?=$invoice_update_gid;?>" placeholder="Invoices Update gid" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Sales Receipts gid</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="sales_receipts_gid" name="sales_receipts_gid" value="<?=$sales_receipts_gid;?>" placeholder="Sales Receipts gid" required="required">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                            <div>

                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include 'footer.php';
?>
