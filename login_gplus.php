<?php
    include 'qwiklee_config.php';
    require_once __DIR__ . '/vendor/autoload.php';
    define('SCOPES', implode(' ', array(
            Google_Service_Sheets::SPREADSHEETS,
            Google_Service_Oauth2::PLUS_LOGIN)
    ));
    $client = new Google_Client();
    $client->setScopes(SCOPES);
    $client->setAccessType('offline');
    $client->setApprovalPrompt("force");
    $client->setAuthConfigFile('client_secret.json');
    $client->setRedirectUri(BASEURL.'login_gplus.php');

    if (!isset($_GET['code'])) {
        $auth_url = $client->createAuthUrl();
        header('Location:'.filter_var($auth_url,FILTER_SANITIZE_URL));
    } else {
        $client->authenticate($_GET['code']);
        $google_oauth =new Google_Service_Oauth2($client);
        $_SESSION['access_token'] = $client->getAccessToken();
        $_SESSION['is_login'] = $google_oauth->userinfo->get()->id;
        $_SESSION['user_id'] = $google_oauth->userinfo->get()->id;
        $_SESSION['username'] = $google_oauth->userinfo->get()->name;
        $_SESSION['email'] = $google_oauth->userinfo->get()->email;
        $_SESSION['profile_pic'] = $google_oauth->userinfo->get()->picture;

        /*----------- GET USER CONFIG -------------------*/
            $user_id = $google_oauth->userinfo->get()->id;
            $email = $google_oauth->userinfo->get()->email;
            $username = $google_oauth->userinfo->get()->name;
            $query = "SELECT * FROM  `qwiklee_users` WHERE `user_id` = '$user_id' LIMIT 1";
            $result = mysqli_query($connection_obj,$query);
            if(mysqli_num_rows($result) == 1) {
                $UserRow = mysqli_fetch_object($result);
                $_SESSION['google_sheet_id'] = trim($UserRow->google_sheet_id);
                $_SESSION['customers_gid'] = trim($UserRow->customers_gid);
                $_SESSION['vendors_gid'] = trim($UserRow->vendors_gid);
                $_SESSION['invoices_gid'] = trim($UserRow->invoices_gid);
                $_SESSION['invoice_update_gid'] = trim($UserRow->invoice_update_gid);
                $_SESSION['sales_receipts_gid'] = trim($UserRow->sales_receipts_gid);
                $_SESSION['payment_gid'] = trim($UserRow->payment_gid);
                $_SESSION['account_gid'] = trim($UserRow->account_gid);
                $_SESSION['attachable_gid'] = trim($UserRow->attachable_gid);
                $_SESSION['bill_gid'] = trim($UserRow->bill_gid);
                $_SESSION['billpayment_gid'] = trim($UserRow->billpayment_gid);
                $_SESSION['class_gid'] = trim($UserRow->class_gid);
                $_SESSION['creditmemo_gid'] = trim($UserRow->creditmemo_gid);
                $_SESSION['departmemt_gid'] = trim($UserRow->departmemt_gid);
                $_SESSION['deposit_gid'] = trim($UserRow->deposit_gid);
                $_SESSION['employee_gid'] = trim($UserRow->employee_gid);
                $_SESSION['estimate_gid'] = trim($UserRow->estimate_gid);
                $_SESSION['item_gid'] = trim($UserRow->item_gid);
                $_SESSION['journalentry_gid'] = trim($UserRow->journalentry_gid);
                $_SESSION['paymentmethods_gid'] = trim($UserRow->paymentmethods_gid);
                $_SESSION['purchase_gid'] = trim($UserRow->purchase_gid);
                $_SESSION['purchaseorders_gid'] = trim($UserRow->purchaseorders_gid);
                $_SESSION['refundreceipt_gid'] = trim($UserRow->refundreceipt_gid);
                $_SESSION['term_gid'] = trim($UserRow->term_gid);
                $_SESSION['timeactivity_gid'] = trim($UserRow->timeactivity_gid);
                $_SESSION['transfer_gid'] = trim($UserRow->transfer_gid);
                $_SESSION['vendorcredit_gid'] = trim($UserRow->vendorcredit_gid);
                
                $update_query = "UPDATE `qwiklee_users` SET `username` = '$username',`email` = '$email' WHERE `user_id` = $user_id ";
                mysqli_query($connection_obj,$update_query);
                $_SESSION['is_qb_login'] = false;
                header('Location:'.BASEURL.'dashboard.php');
            }else{
                $created_at = date("Y-m-d H:i:s");
                $insert_query = "INSERT INTO `qwiklee_users`(`user_id`,`email`,`username`, `google_sheet_id`,`customers_gid`,`vendors_gid`,`invoices_gid`,`invoice_update_gid`,`sales_receipts_gid`,`created_at`)
                                                     VALUES ('$user_id','$email','$username','','','','','','','$created_at')";
				mysqli_query($connection_obj,$insert_query);
                $_SESSION['is_qb_login'] = false;
                header('location:'.BASEURL.'create_gs.php');
            }
        /*----------- GET USER CONFIG -------------------*/
    }
