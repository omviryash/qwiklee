<!-- Main sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=isset($_SESSION['profile_pic']) && $_SESSION['profile_pic'] != ''?$_SESSION['profile_pic']:BASEURL.'dist/img/user2-160x160.jpg'?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?=isset($_SESSION['username']) && $_SESSION['username'] != ''?$_SESSION['username']:'Qwiklee User'?></p>
                <a href="javascript:void(0);"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <?/*=$currUrl == ''?'class="active"':'';*/?>
            <li>
                <a href="<?=BASEURL;?>dashboard.php">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="<?=BASEURL;?>set_configuration.php">
                    <i class="fa fa-dashboard"></i> <span>Set Configuration</span>
                </a>
            </li>
            <?php if(isset($_SESSION['is_qb_login']) && $_SESSION['is_qb_login'] ==  true) {?>
                <li>
                    <a href="<?=BASEURL;?>qb/disconnect.php">
                        <i class="fa fa-dashboard"></i> <span>Disconnect To QuickBooks</span>
                    </a>
                </li>
                <li>
                    <a href="<?=BASEURL;?>qb/customers_view.php">
                        <i class="fa fa-dashboard"></i> <span>Customers</span>
                    </a>
                </li>

                <li>
                    <a href="<?=BASEURL;?>qb/vendors_view.php">
                        <i class="fa fa-dashboard"></i> <span>Vendors</span>
                    </a>
                </li>
                <li>
                    <a href="<?=BASEURL;?>qb/invoices_view.php">
                        <i class="fa fa-dashboard"></i> <span>Invoices</span>
                    </a>
                </li>
                <li>
                    <a href="<?=BASEURL;?>qb/sales_receipts_view.php">
                        <i class="fa fa-dashboard"></i> <span>Sales Receipts</span>
                    </a>
                </li>
				<li>
					<a href="<?=BASEURL;?>qb/payment_view.php">
						<i class="fa fa-dashboard"></i> <span>Payment</span>
					</a>
				</li>
                <li>
                    <a href="<?=BASEURL;?>qb/qb_to_gs.php">
                        <i class="fa fa-dashboard"></i> <span>Quickbooks To Google Sheet</span>
                    </a>
                </li>
            <?php } else {?>
                <li>
                    <a href="javascript:void(0);">
                        <ipp:connectToIntuit></ipp:connectToIntuit>
                    </a>
                </li>
            <?php }?>
            <li>
                <a href="javascript:void(0);">
                    <i class="fa fa-dashboard"></i> <span>Qwiklee coming :  xero !</span>
                </a>
            </li>
             <li>
                    <a href="<?=BASEURL;?>qb/account_view.php">
                        <i class="fa fa-dashboard"></i> <span>Account</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/attachable_view.php">
                        <i class="fa fa-dashboard"></i> <span>Attachable</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/bill_view.php">
                        <i class="fa fa-dashboard"></i> <span>Bill</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/billpayment_view.php">
                        <i class="fa fa-dashboard"></i> <span>BillPayment</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/class_view.php">
                        <i class="fa fa-dashboard"></i> <span>Class</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/creditmemo_view.php">
                        <i class="fa fa-dashboard"></i> <span>CreditMemo</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/department_view.php">
                        <i class="fa fa-dashboard"></i> <span>Department</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/deposit_view.php">
                        <i class="fa fa-dashboard"></i> <span>Deposit</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/employee_view.php">
                        <i class="fa fa-dashboard"></i> <span>Employee</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/estimate_view.php">
                        <i class="fa fa-dashboard"></i> <span>Estimate</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/invoiceupdate_view.php">
                        <i class="fa fa-dashboard"></i> <span>InvoiceUpdate</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/item_view.php">
                        <i class="fa fa-dashboard"></i> <span>Item</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/journalentry_view.php">
                        <i class="fa fa-dashboard"></i> <span>JournalEntry</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/payment_view.php">
                        <i class="fa fa-dashboard"></i> <span>Payment</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/paymentmethods_view.php">
                        <i class="fa fa-dashboard"></i> <span>PaymentMethods</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/purchase_view.php">
                        <i class="fa fa-dashboard"></i> <span>Purchase</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/purchaseorder_view.php">
                        <i class="fa fa-dashboard"></i> <span>PurchaseOrder</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/refundreceipt_view.php">
                        <i class="fa fa-dashboard"></i> <span>RefundReceipt</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/term_view.php">
                        <i class="fa fa-dashboard"></i> <span>Term</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/timeactivity_view.php">
                        <i class="fa fa-dashboard"></i> <span>TimeActivity</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/transfer_view.php">
                        <i class="fa fa-dashboard"></i> <span>Transfer</span>
                    </a>
             </li>
             <li>
                    <a href="<?=BASEURL;?>qb/vendorcredit_view.php">
                        <i class="fa fa-dashboard"></i> <span>VendorCredit</span>
                    </a>
             </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<!-- /main sidebar -->
