        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Morris.js charts -->
    <script src="<?=BASEURL?>bootstrap/js/raphael-min.js"></script>
    <!-- Sparkline -->
    <script src="<?=BASEURL?>plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?=BASEURL?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?=BASEURL?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?=BASEURL?>plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="<?=BASEURL?>bootstrap/js/moment.min.js"></script>
    <script src="<?=BASEURL?>plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="<?=BASEURL?>plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?=BASEURL?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?=BASEURL?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!--Page Specific Js-->
    <script src="<?=BASEURL?>plugins/iCheck/icheck.min.js"></script>
    <!--Page Specific Js-->
    <!-- FastClick -->
    <script src="<?=BASEURL?>plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=BASEURL?>dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=BASEURL?>dist/js/demo.js"></script>
    </body>
</html>